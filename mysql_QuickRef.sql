
# MYSQL
# mysql onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g')
# mysql $(grep database /onapp/interface/config/database.yml | awk 'END{print $NF}') -p$(grep password /onapp/interface/config/database.yml | awk 'END{print $NF}')


### SHOW HYPERVISORS
# mysql -N onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select ip_address from hypervisors;'
### SHOW HYPERVISORS+BACKUP-SERVERS
# mysql -N onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select ip_address from hypervisors;select ip_address from backup_servers;'
### Show all failure counts
# mysql onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select id,ip_address,failure_count from hypervisors;'
### SHOW ONAPP VERSION
# for HV in `mysql -N onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select ip_address from hypervisors;' `; do echo -n $HV: ; ssh root@$HV rpm -qa|grep onapp-hv |sort | xargs echo ; done
### RUN CMD (all HVs)
# for HV in `mysql -N onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select ip_address from hypervisors;' `; do echo === $HV === ; ssh root@$HV '#' ; done
### RUN CMD (BK + all HVs)
# for IP in `mysql -N onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/"//g') -e 'select ip_address from backup_servers; select ip_address from hypervisors;' `; do echo === $IP === ; ssh root@$IP '#' ; done

# show transactions "running"
select id,parent_type,parent_id,action,status,actor,params,created_at,updated_at,dependent_transaction_id,pid,user_id,identifier,allowed_cancel,start_after,started_at from transactions where status='running';'


'
SELECT INET_NTOA(ip_number) AS IPAddressDotted, ip_address_joins.network_interface_id  FROM ip_addresses INNER JOIN ip_address_joins ON ip_address_joins.ip_address_id=ip_addresses.id wNNER JOIN here INETNTOA(ip_number)='91.227.221.123'\G

select * from network_interfaces where id in (select network_interface_id from ip_address_joins where ip_address_id in (select id from ip_addresses where ip_number=INET_ATON('159.253.7.21')));

# for v.2.3.x
SELECT virtual_machines.id AS VM_ID, virtual_machines.identifier AS VM_identifier, network_interfaces.id AS IfaceID, network_interfaces.label AS IfaceLabel, network_interfaces.identifier AS IfaceIdentifier, network_interfaces.mac_address AS MAC, network_interfaces.primary AS IfacePrimary, INET_NTOA(ip_addresses.ip_number) AS IPAddressDotted  
FROM ip_addresses 
INNER JOIN 
   ip_address_joins ON ip_address_joins.ip_address_id=ip_addresses.id 
INNER JOIN 
   network_interfaces ON ip_address_joins.network_interface_id=network_interfaces.id
INNER JOIN
   virtual_machines ON network_interfaces.virtual_machine_id=virtual_machines.id
WHERE INET_NTOA(ip_addresses.ip_number)='10.0.0.1' ;
''

# for 3.0.x
SELECT virtual_machines.id AS VM_ID, virtual_machines.identifier AS VM_identifier, network_interfaces.id AS IfaceID, network_interfaces.label AS IfaceLabel, network_interfaces.identifier AS IfaceIdentifier, network_interfaces.mac_address AS MAC, network_interfaces.primary AS IfacePrimary, INET_NTOA(ip_addresses.address) AS IPAddressDotted  
FROM ip_addresses 
INNER JOIN 
   ip_address_joins ON ip_address_joins.ip_address_id=ip_addresses.id 
INNER JOIN 
   network_interfaces ON ip_address_joins.network_interface_id=network_interfaces.id
INNER JOIN
   virtual_machines ON network_interfaces.virtual_machine_id=virtual_machines.id
WHERE INET_NTOA(ip_addresses.address)='10.0.0.1' ;

#######################
####  VM with Net
#######################
SELECT 
  vm.id AS VM_ID, 
  vm.identifier AS VM_identifier, 
  vm.hypervisor_id AS HV_ID, 
  hv.label AS HV_IP, 
  hv.ip_address AS HV_IP, 
  ni.id AS IfaceID, 
  ni.label AS IfaceLabel, 
  ni.identifier AS IfaceIdentifier, 
  ni.mac_address AS MAC, 
  ni.primary AS IfacePrimary, 
  INET_NTOA(ia.address) AS IPAddressDotted  
FROM ip_addresses AS ia
INNER JOIN 
   ip_address_joins AS iaj ON iaj.ip_address_id=ia.id 
INNER JOIN 
   network_interfaces AS ni ON iaj.network_interface_id=ni.id
INNER JOIN
   virtual_machines AS vm ON ni.virtual_machine_id=vm.id
INNER JOIN
   hypervisors AS hv ON vm.hypervisor_id=hv.id
WHERE

	INET_NTOA(ia.address)='10.0.0.1' ;
	vm.id=477 ;
	vm.identifier='hzadihw91x2exi' ;

#######################
####  VM with Disk
#######################

# mysql onapp -p$(grep password /onapp/interface/config/database.yml | awk '{print $2}' | head -n 1 | sed 's/["|'\'']//g')
# mysql $(grep database /onapp/interface/config/database.yml | awk 'END{print $NF}') -p$(grep password /onapp/interface/config/database.yml | awk 'END{print $NF}')
#'

SELECT 
  vm.id AS VM_ID, 
  vm.identifier AS VM_identifier, 
  vm.hypervisor_id AS HV_ID, 
  hv.label AS HV_IP, 
  hv.ip_address AS HV_IP, 
  dsk.id AS Disk_ID,
  dsk.identifier AS DiskIdentifier
FROM 
   virtual_machines AS vm
INNER JOIN
   hypervisors AS hv ON vm.hypervisor_id=hv.id
INNER JOIN
   disks AS dsk ON dsk.virtual_machine_id=vm.id
INNER JOIN
    data_stores AS dtst ON dsk.data_store_id=dtst.id
WHERE
   vm.identifier='' ;


	INET_NTOA(ia.address)='10.0.0.1' ;
	vm.id=477 ;
	vm.identifier='hzadihw91x2exi' ;

select id from disks where virtual_machine_id not in (select distinct id from virtual_machines);
select id from disks where data_store_id not in (select distinct id from data_stores);
select id from backups where template_id not in (select distinct id from templates);
select id from backups where user_id not in (select distinct id from users);
select id from backups where target_id not in (select distinct id from disks);

SELECT table_schema "Data Base Name", SUM( data_length + index_length) / 1024 / 1024 "Data Base Size in MB" FROM information_schema.TABLES GROUP BY table_schema ;

SELECT TABLE_NAME, table_rows, data_length, index_length, round(((data_length + index_length) / 1024 / 1024),2) "Size in MB" FROM information_schema.TABLES WHERE table_schema = "onapp";

# Recommended InnoDB Buffer Pool Size (RIBPS) based on all InnoDB Data
SELECT CEILING(Total_InnoDB_Bytes*1.6/POWER(1024,3)) RIBPS FROM (SELECT SUM(data_length+index_length) Total_InnoDB_Bytes FROM information_schema.tables WHERE engine='InnoDB') A;


# Change password
ALTER USER 'userName'@'localhost' IDENTIFIED BY 'New-Password-Here';

# Change password using mysql db (for mysql database server version 5.7.5 or older:
SET PASSWORD FOR 'user-name-here'@'hostname' = PASSWORD('new-password');

# For mysql database server version 5.7.6 or newer use the following syntax:
ALTER USER 'user'@'hostname' IDENTIFIED BY 'newPass';

#You can also use the following sql syntax:
UPDATE mysql.user SET Password=PASSWORD('new-password-here') WHERE USER='user-name-here' AND Host='host-name-here';

#In this example, change a password for a user called tom:
SET PASSWORD FOR 'tom'@'localhost' = PASSWORD('foobar');
# or
UPDATE mysql.user SET Password=PASSWORD('foobar') WHERE USER='tom' AND Host='localhost';
