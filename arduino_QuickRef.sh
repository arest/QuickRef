#define CSn             2

#define CLR(x,y) (x&=(~(1<<y)))
#define SET(x,y) (x|=(1<<y))

int pins[8] = {PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7};
 
void dw(int pin, int st){ //digital write
  if(st==LOW) PORTD &= ~_BV(pins[pin]);
  if(st==HIGH) PORTD |= _BV(pins[pin]);
}
 
int dr(int pin){ //digital read
  if((PIND & (1<<pins[pin])) == 64) return HIGH;
  if((PIND & (1<<pins[pin])) == 0) return LOW;
}
 
void pm(int pin, int st){ //pinmode
  if(st==OUTPUT) DDRD |= (1<<pins[pin]);
  if(st==INPUT)  DDRD &= ~(1<<pins[pin]);
}

DDRB  = (1<<CSn); //включаем на выход PortB.2
PORTB = (1<<CSn); //записали 1 на нашем пине

DDRD = B11111110;        // назначает выводы Arduino 1-7 выходными, 
					    //вывод 0- входным
DDRD = DDRD | B11111100; //это менее рискованно - назначает выводы со 2 по 7
                        // выходными, не изменяя значений на выводах 0 и 1

PORTD = B10101000; // устанавливает HIGH на цифровых выводах 7,5,3


# vim: syntax=arduino ts=4 sw=4 sts=4 sr noet
