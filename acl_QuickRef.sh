



###########  getfacl  ################
# Примеры использования:

    getfacl * - отобразит права ACL для всех объектов в текущем каталоге;
    getfacl sobaka.txt - отобразить ACL для файла sobaka.txt;
    getfacl kartosh* - отобразит ACL для всех файлов в текущем каталоге, которые начинаются на kartosh;
    getfacl -R * - отобразит ACL для всех объектов (включая подкаталоги и их содержимое) текущего каталога.


## file: qwert   - Имя файла
## owner: root   - Владелец файла (основные права Unix)
## group: root   - Группа файла (основные права Unix)
##user::rwx       - Права для владельца файла (основные права Unix)
#user:child:rw-  - Права ACL для пользователя child
#group::r--      - Права для группы файла (основные права Unix)
#mask::rw-       - Эффективная маска
#other::---      - Права для пользователя "все остальные" 


###########  setfacl  ################

# Списки ACL можно задать:

#    На уровне пользователей - назначаются ACL конкретным пользователям;
#    На уровне групп - назначаются ACL конкретным группам;
#    С помощью маски эффективных прав - ограничение максимальных прав для пользователей и/или групп;
#    Для пользователей, не включённых в группу данного файла - это т.н. пользователь «Все остальные»;
#



root@sytserver:/media/Work/test# setfacl -m u:allexserv:rwx qwert
root@sytserver:/media/Work/test# getfacl qwert
# file: qwert
# owner: root
# group: root
user::rwx
user:allexserv:rwx
user:child:rw-
group::r--
mask::rwx
other::---


 Обратите внимание как изменилась эффективная маска. Все ACL права сложились: у пользователя child rw- и у пользователя allexserv rwx. Но маска вовсе не разрешает пользователю child иметь право на выполнение (x) файла qwert. Посмотрите, что произойдет если принудительно изменить эффективную маску:

root@sytserver:/media/Work/test# setfacl -m m:r qwert
root@sytserver:/media/Work/test# getfacl qwert
# file: qwert
# owner: root
# group: root
user::rwx
user:allexserv:rwx	#effective:r--
user:child:rw-		#effective:r--
group::r--
mask::r--
other::---


 Теперь давайте удалим с файла qwert права ACL для пользователя allexserv:

root@sytserver:/media/Work/test# setfacl -x u:allexserv qwert
root@sytserver:/media/Work/test# getfacl qwert
# file: qwert
# owner: root
# group: root
user::rwx
user:child:rw-		#effective:r--
group::r--
mask::r--
other::---


 Назначаем ACL права сразу для двух пользователей, перечислив их через запятую:

root@sytserver:/media/Work/test# setfacl -m u:allexserv:rwx,u:mysql:rwx Proverka
root@sytserver:/media/Work/test# getfacl Proverka
# file: Proverka
# owner: child
# group: children
user::rwx
user:mysql:rwx
user:allexserv:rwx
group::---
mask::rwx
other::---

Теперь назначим ACL по умолчанию. При назначении ACL по умолчанию, также требуется в обязательном порядке указывать и основные права в виде u::rwx,g::-,o::- где u - user - владелец, g - group - группа, o - other - все остальные:

root@sytserver:/media/Work/test# setfacl -d -m u::rwx,g::-,o::-,u:allexserv:rwx,u:mysql:rwx Proverka 
root@sytserver:/media/Work/test# getfacl Proverka
# file: Proverka
# owner: child
# group: children
user::rwx
user:mysql:rwx
user:allexserv:rwx
group::---
mask::rwx
other::---
default:user::rwx
default:user:mysql:rwx
default:user:allexserv:rwx
default:group::---
default:mask::rwx
default:other::---

Видно, что появились строки начинающиеся с default. Это и есть права по умолчанию, которые будут принимать все создаваемые внутри объекты. Проверим, создав пустой файл myfile.txt и подкаталог MyKat в каталоге Proverka:

root@sytserver:/media/Work/test/Proverka# touch myfile.txt
root@sytserver:/media/Work/test/Proverka# mkdir MyKat
root@sytserver:/media/Work/test/Proverka# getfacl *
# file: myfile.txt
# owner: root
# group: root
user::rw-
user:mysql:rwx		#effective:rw-
user:allexserv:rwx      #effective:rw-
group::---
mask::rw-
other::---

# file: MyKat
# owner: root
# group: root
user::rwx
user:mysql:rwx
user:allexserv:rwx
group::---
mask::rwx
other::---
default:user::rwx
default:user:mysql:rwx
default:user:allexserv:rwx
default:group::---
default:mask::rwx
default:other::---


 Обратите внимание, что каталог MyKat не только приобрел ACL, но и также приобрел и ACL по умолчанию. Т.е. те объекты, которые будут создаваться в подкаталоге MyKat тоже будут наследовать ACL по умолчанию.

# Удалить права по умолчанию можно: 
setfacl -k Proverka.

# Если нужно также удалить права по умолчанию и в подкаталогах, то добавьте ключ -R (рекурсия): 
setfacl -R -k /media/Work/test/Proverka . 

#Копирование ACL прав с одного объекта на другой.
getfacl file1 | setfacl --set-file=- file2

#В этом случае права на file2 не заменяются как при использовании - -set, а добавляются к уже существующим правам ACL.
getfacl file1 | setfacl -M- file2

#  В этом примере getfacl получает все права которые вы установили на каталог dir и устанавливает их на этот же каталог dir делая их правами по умолчанию. Очень удобно. Обратите внимание на ключ - -access у команды getfacl. При вызове getfacl без параметров, она отображает все права ACL, включая права по умолчанию. Здесь же, ключ - -access заставляет getfacl показать только права ACL на каталог, а права по умолчанию (если таковые имеются у каталога) - скрыть. Обратный ключ - это ключ -d:

 getfacl -d Proverka	
# заставит getfacl показать только права по умолчанию, а права ACL на сам каталог - скрыть. Кстати, в нашем примере, с каталогом Proverka, на который мы назначали права по умолчанию, чтобы не писать строку:
 setfacl -d -m u::rwx,g::-,o::-,u:allexserv:rwx,u:mysql:rwx Proverka  

# можно было бы воспользоваться именно этой фишечкой, как то так:
 getfacl --access Proverka | setfacl -d -M- Proverka

============================================================================================================================
--set или --set file* #	-	Устанавливает новые указанные права ACL, удаляя все существующие.
		      #		Необходимо, чтобы наравне с задаваемыми правилами ACL были также указаны
		      #		стандартные права Unix, в противном случае будет давать ошибку;

-m или -M file* # 	Модифицирует указанные ACL на объекте. Другие существующие ACL сохраняются.
-x или -X file* #	Удаляет указанные ACL права с объекта. Стандартные права Unix не изменяются.
-b 	# Удаляет все ACL права с объекта, сохраняя основные права;
-k 	# Удаляет с объекта ACL по умолчанию. Если таковых на объекте нет, предупреждение об этом выдаваться не будет;
-d 	- Устанавливает ACL по умолчанию на объект.
–restore=file 	- Восстанавливает ACL права на объекты из ранее созданного файла с правами.5)
-R 	- Рекурсивное назначение (удаление) прав, тобишь пройтись по всем подкаталогам.
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
