###############################
##### Capturing remote HV logs
###############################
#  1) rsyslog
# 	Add to /etc/rsyslog.conf or /etc/rsyslog.d/onapp.conf:
$ModLoad imudp
$UDPServerRun 514

	# 	Add to /etc/rsyslog.conf or /etc/rsyslog.d/onapp.conf:
if $fromhost-ip startswith '10.0.0.' then /var/log/messages-hv.log
	# 	stop processing the message after it was written to the log
& ~
	
service rsyslogd restart

#  2) syslog
	Edit the /etc/sysconfig/syslog, change variable SYSLOGD_OPTIONS to
		SYSLOGD_OPTIONS="-m 0 -r -x"
	service syslog restart

#====================================================================================
###############################
##### 
###############################
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
