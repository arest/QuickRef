   pkcs15-init --erase-card -p rutoken_ecp -l "Arest's Token"
   pkcs15-init --create-pkcs15 --so-pin "oso3agore2af" --so-puk ""
   pkcs15-init --store-pin --label "User PIN" --auth-id 02 --pin "3128443" --puk "" --so-pin "oso3agore2af" --finalize

   pkcs15-init -S ./ArestRootCA.key.pem -a 02 --authority -l "Arest Root CA Private Key"
   pkcs15-tool -D
   pkcs15-init -X ./ArestRootCA.cert.pem -a 02 --authority -l "Arest Root CA certificate" -i ac21f69c5589c73855e41373b6cbfbeeb315f14d
   pkcs15-init -S ./ArestIntermediateCA.key.pem -a 02 --authority -l "Arest Intermediate CA Private Key"
   pkcs15-tool -D
   pkcs15-init -X ./ArestIntermediateCA.cert.pem -a 02 --authority -l "Arest Intermediate CA certificate" -i 273216e950919a4d582247f83010b1eda65bfe07
   pkcs15-init -S dr.arest@gmail.com.key.pem -a 02 -l "Orest Pazdriy" --extractable
   pkcs15-tool -D
   pkcs15-init -X ./dr.arest@gmail.com.cert.pem -a 02 -l "Orest Pazdriy certificate" -i 578ee129c852f70d396e5244c6dfa9b35aa45206



=======================================================================================================

pkcs15-init --erase-card
pkcs15-init --create-pkcs15 --so-pin "87654321" --so-puk ""
pkcs15-init --store-pin --label "User PIN" --auth-id 02 --pin "12345678" --puk ""

opensc-explorer
#Using reader with a card: Rutoken S driver
#OpenSC [3F00]> apdu 80:8A:00:00
#OpenSC [3F00]> mkdir 3F00 0
#OpenSC [3F00]> exit

# And run the Initialize procedure 


pkcs15-init --erase-card -p rutoken_ecp
pkcs15-init --create-pkcs15 --so-pin "87654321" --so-puk ""
pkcs15-init --store-pin --label "User PIN" --auth-id 02 --pin "12345678" --puk "" --so-pin "87654321" --finalize
pkcs15-tool -D
#Using reader with a card: Aktiv Rutoken ECP 00 00
#PKCS#15 Card [Rutoken ECP]:
#	Version        : 0
#	Serial number  : 000000002CDB7256
#	Manufacturer ID: Aktiv Co.
#	Last update    : 20121114083252Z
#	Flags          : EID compliant

#==================== 
#Generate private key
#====================
pkcs15-init -G rsa/1024 --auth-id 02 --label "My Private Key" --public-key-label "My Public Key"
#Using reader with a card: Aktiv Rutoken ECP 00 00
#User PIN [User PIN] required.
#Please enter User PIN [User PIN]: 
pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
#
#===========================
#Generate openssl certificate request
#====================================
$ pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
$ openssl
OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine
#OpenSSL> 
#OpenSSL> req -engine pkcs11 -new -key slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181 -keyform engine -out req.csr -subj "/C=RU/ST=Moscow/L=Moscow/O=KORUS/OU=IT/CN=Sergey Safarov/emailAddress=s.safarov@mail.com"
#engine "pkcs11" set.
#PKCS#11 token PIN: 

#===============================
#Generate selfsigned certificate
#===============================
$ pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
$ openssl
#OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine
#OpenSSL> req -engine pkcs11 -x509 -new -key slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181  -keyform engine -out ca.crt -subj "/C=RU/ST=Moscow/L=Moscow/O=KORUS/OU=IT/CN=Sergey Safarov/emailAddress=s.safarov@mail.com"
#engine "pkcs11" set.
#PKCS#11 token PIN: 

#=====================
#Sign certificate
#=====================
$ pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
$ openssl
#OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine
#OpenSSL> ca -engine pkcs11 -keyfile slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181  -keyform engine -cert ca.crt -in req.csr -out tester.crt
#Using configuration from /etc/pki/tls/openssl.cnf
#engine "pkcs11" set.
#PKCS#11 token PIN: 
#Check that the request matches the signature
#Signature ok
#Certificate Details:
#        Serial Number: 1 (0x1)
#        Validity
#            Not Before: Nov 14 09:04:44 2012 GMT
#            Not After : Nov 14 09:04:44 2013 GMT
#        Subject:
#            countryName               = RU
#            stateOrProvinceName       = Moscow
#            organizationName          = KORUS
#            organizationalUnitName    = IT
#            commonName                = Sergey Safarov
#            emailAddress              = s.safarov@mail.com
#        X509v3 extensions:
#            X509v3 Basic Constraints: 
#                CA:FALSE
#            Netscape Comment: 
#                OpenSSL Generated Certificate
#            X509v3 Subject Key Identifier: 
#                DD:47:C4:DB:57:4B:04:BB:67:82:5A:88:DF:93:1C:6D:E2:CB:58:0F
#            X509v3 Authority Key Identifier: 
#                DirName:/C=RU/ST=Moscow/L=Moscow/O=KORUS/OU=IT/CN=Sergey Safarov/emailAddress=s.safarov@mail.com
#                serial:8F:30:77:60:57:FA:D0:06

#=====================
#Store certificate
#====================
pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
$ pkcs15-init --store-certificate tester.crt --auth-id 02 --id 04830838b7c9752bd0e90c96a88b989a80f45181 --format pem
$ pkcs15-tool --read-certificate 04830838b7c9752bd0e90c96a88b989a80f45181
#Using reader with a card: Aktiv Rutoken ECP 00 00
pkcs15-init --erase-card -p rutoken_ecp
pkcs15-init --create-pkcs15 --so-pin "87654321" --so-puk ""
pkcs15-init --store-pin --label "User PIN" --auth-id 02 --pin "12345678" --puk "" --so-pin "87654321" --finalize
pkcs15-tool -D

# Generate private key


pkcs15-init -G rsa/1024 --auth-id 02 --label "Orest Pazdriy Private Key" --public-key-label "Orest Pazdriy Public Key"
pkcs15-tool --list-keys

#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
#
#Generate openssl certificate request

pkcs15-tool --list-keys

#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}

openssl

engine dynamic -pre SO_PATH:/usr/lib/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so

#OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so

#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine
#OpenSSL> 


export ROOTCA_ID=$(pkcs15-tool --list-keys 2>/dev/null |awk /ID/{getline;print $3})
expect ~/bin/openssl_pkcs11.expect $ROOTCA_ID


#OpenSSL> req -engine pkcs11 -new -key slot_0-id_${ROOTCA_ID} -keyform engine -out req.csr -subj "/C=UA/ST=Lviv/L=Lviv/O=Arest Inc./OU=IT/CN=Orest Pazdriy/emailAddress=dr.arest@gmail.com"

req -engine pkcs11 -new -key slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181 -keyform engine -out req.csr -subj "/C=UA/O=Arest Inc./CN=Arest Inc. Internal Root CA"

#engine "pkcs11" set.
#PKCS#11 token PIN: 
#OpenSSL> quit
cat req.csr 

#If you have errors when loading engine – check the location of directory of engine_pkcs11.so
#(for example: /usr/lib/engines/engine_pkcs11.so ) and use it further when loading engine.
#Generate selfsigned certificate

pkcs15-tool --list-keys

#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}

openssl

#OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine

#OpenSSL> req -engine pkcs11 -x509 -new -key slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181  -keyform engine -out ca.crt -subj "/C=UA/ST=Lviv/L=Lviv/O=Arest Inc./OU=IT/CN=Orest Pazdriy/emailAddress=dr.arest@gmail.com"
#engine "pkcs11" set.
#PKCS#11 token PIN: 
#OpenSSL> quit
cat ca.crt
#-----BEGIN CERTIFICATE-----
#MIICiTCCAfICCQCPMHdgV/rQBjANBgkqhkiG9w0BAQUFADCBiDELMAkGA1UEBhMC
#UlUxDzANBgNVBAgMBk1vc2NvdzEPMA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVL
#T1JVUzELMAkGA1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJ
#KoZIhvcNAQkBFhJzLnNhZmFyb3ZAbWFpbC5jb20wHhcNMTIxMTE0MDg1MTMyWhcN
#MTIxMjE0MDg1MTMyWjCBiDELMAkGA1UEBhMCUlUxDzANBgNVBAgMBk1vc2NvdzEP
#MA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVLT1JVUzELMAkGA1UECwwCSVQxFzAV
#BgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJKoZIhvcNAQkBFhJzLnNhZmFyb3ZA
#bWFpbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAKljQszR4EMoBOtH
#jt+gWfNWsxUSX6h4vHx98BQNEiwrIgsEMQ6uGPfAP/yo1IFeXQJNdEvWM8n8KaPL
#qyL4Am1UJue6F7Ys/TriRxuPXt66sC4CE/AoyUuGNyGw+gRyM3bZ9BgCJ75c35q1
#O1Zq4Vqc5Xq9qVUazJuhJxs/INt5AgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAlmet
#AeeNkdUjCiP3nk8PJ5lW8d+ohl55W6gsi4pRvLwXH/CsKzU3scNbPKnEQz1FGpfZ
#xHp+LB1jZSUci1r6saQKkDFLXLQqIedRhR2Bevx5msw+ydM3GwwRW9K0IumwrYwp
#9IGRsBOT1s7eTZfYNURmqdhP5hIdgo3dJ4utr1c=
#-----END CERTIFICATE-----

#Sign certificate

pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}

openssl

#OpenSSL> engine dynamic -pre SO_PATH:/usr/lib/engines/engine_pkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:opensc-pkcs11.so
#(dynamic) Dynamic engine loading support
#[Success]: SO_PATH:/usr/lib/openssl/engines/engine_pkcs11.so
#[Success]: ID:pkcs11
#[Success]: LIST_ADD:1
#[Success]: LOAD
#[Success]: MODULE_PATH:opensc-pkcs11.so
#Loaded: (pkcs11) pkcs11 engine

#OpenSSL> ca -engine pkcs11 -keyfile slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181  -keyform engine -cert ca.crt -in req.csr -out tester.crt

#Using configuration from /etc/pki/tls/openssl.cnf
#engine "pkcs11" set.
#PKCS#11 token PIN: 
#Check that the request matches the signature
#Signature ok
#Certificate Details:
#        Serial Number: 1 (0x1)
#        Validity
#            Not Before: Nov 14 09:04:44 2012 GMT
#            Not After : Nov 14 09:04:44 2013 GMT
#        Subject:
#            countryName               = RU
#            stateOrProvinceName       = Moscow
#            organizationName          = KORUS
#            organizationalUnitName    = IT
#            commonName                = Sergey Safarov
#            emailAddress              = s.safarov@mail.com
#        X509v3 extensions:
#            X509v3 Basic Constraints: 
#                CA:FALSE
#            Netscape Comment: 
#                OpenSSL Generated Certificate
#            X509v3 Subject Key Identifier: 
#                DD:47:C4:DB:57:4B:04:BB:67:82:5A:88:DF:93:1C:6D:E2:CB:58:0F
#            X509v3 Authority Key Identifier: 
#                DirName:/C=RU/ST=Moscow/L=Moscow/O=KORUS/OU=IT/CN=Sergey Safarov/emailAddress=s.safarov@mail.com
#                serial:8F:30:77:60:57:FA:D0:06
#
#Certificate is to be certified until Nov 14 09:04:44 2013 GMT (365 days)
#Sign the certificate? [y/n]:y
#
#1 out of 1 certificate requests certified, commit? [y/n]y
#Write out database with 1 new entries
#Data Base Updated
#OpenSSL> quit
cat tester.crt
#Certificate:
#Data:
#Version: 3 (0×2)
#Serial Number: 1 (0×1)
#Signature Algorithm: sha1WithRSAEncryption
#Issuer: C=RU, ST=Moscow, L=Moscow, O=KORUS, OU=IT, CN=Sergey Safarov/emailAddress=s.safarov@mail.com
#Validity
#Not Before: Nov 14 09:04:44 2012 GMT
#Not After : Nov 14 09:04:44 2013 GMT
#Subject: C=RU, ST=Moscow, O=KORUS, OU=IT, CN=Sergey Safarov/emailAddress=s.safarov@mail.com
#Subject Public Key Info:
#Public Key Algorithm: rsaEncryption
#Public-Key: (1024 bit)
#Modulus:
#00:a9:63:42:cc:d1:e0:43:28:04:eb:47:8e:df:a0:
#59:f3:56:b3:15:12:5f:a8:78:bc:7c:7d:f0:14:0d:
#12:2c:2b:22:0b:04:31:0e:ae:18:f7:c0:3f:fc:a8:
#d4:81:5e:5d:02:4d:74:4b:d6:33:c9:fc:29:a3:cb:
#ab:22:f8:02:6d:54:26:e7:ba:17:b6:2c:fd:3a:e2:
#47:1b:8f:5e:de:ba:b0:2e:02:13:f0:28:c9:4b:86:
#37:21:b0:fa:04:72:33:76:d9:f4:18:02:27:be:5c:
#df:9a:b5:3b:56:6a:e1:5a:9c:e5:7a:bd:a9:55:1a:
#cc:9b:a1:27:1b:3f:20:db:79
#Exponent: 65537 (0×10001)
#X509v3 extensions:
#X509v3 Basic Constraints:
#CA:FALSE
#Netscape Comment:
#OpenSSL Generated Certificate
#X509v3 Subject Key Identifier:
#DD:47:C4:DB:57:4B:04:BB:67:82:5A:88:DF:93:1C:6D:E2:CB:58:0F
#X509v3 Authority Key Identifier:
#DirName:/C=RU/ST=Moscow/L=Moscow/O=KORUS/OU=IT/CN=Sergey Safarov/emailAddress=s.safarov@mail.com
#serial:8F:30:77:60:57:FA:D0:06
#Signature Algorithm: sha1WithRSAEncryption 14:4b:39:32:81:37:aa:95:8f:db:f8:42:64:21:32:6f:11:ad: 8e:c1:ef:bf:20:93:e2:6f:66:80:fc:f6:af:ad:5c:80:6f:98: 1f:28:ea:14:87:f9:6c:5d:59:2c:0a:42:fd:a1:ed:34:f5:4b: c5:7c:5e:2f:16:48:27:a1:c5:b8:0d:f3:64:d0:7f:f6:55:fc: 36:3c:30:bf:7f:e9:62:23:e6:4f:45:76:43:9f:ae:a7:b4:5a: ca:e7:98:f3:6e:09:91:58:f3:4b:6f:36:e3:88:fc:48:54:95: a7:be:58:b8:97:86:5c:57:7e:cb:c4:0a:f2:d6:ac:c3:90:11: 2b:00
#
#BEGIN CERTIFICATE——-
#MIIDfjCCAuegAwIBAgIBATANBgkqhkiG9w0BAQUFADCBiDELMAkGA1UEBhMCUlUx
#DzANBgNVBAgMBk1vc2NvdzEPMA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVLT1JV
#UzELMAkGA1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJKoZI
#hvcNAQkBFhJzLnNhZmFyb3ZAbWFpbC5jb20wHhcNMTIxMTE0MDkwNDQ0WhcNMTMx
#MTE0MDkwNDQ0WjB3MQswCQYDVQQGEwJSVTEPMA0GA1UECAwGTW9zY293MQ4wDAYD
#VQQKDAVLT1JVUzELMAkGA1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92
#MSEwHwYJKoZIhvcNAQkBFhJzLnNhZmFyb3ZAbWFpbC5jb20wgZ8wDQYJKoZIhvcN
#AQEBBQADgY0AMIGJAoGBAKljQszR4EMoBOtHjt+gWfNWsxUSX6h4vHx98BQNEiwr
#IgsEMQ6uGPfAP/yo1IFeXQJNdEvWM8n8KaPLqyL4Am1UJue6F7Ys/TriRxuPXt66
#sC4CE/AoyUuGNyGw+gRyM3bZ9BgCJ75c35q1O1Zq4Vqc5Xq9qVUazJuhJxs/INt5
#AgMBAAGjggEGMIIBAjAJBgNVHRMEAjAAMCwGCWCGSAGG+EIBDQQfFh1PcGVuU1NM
#IEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQU3UfE21dLBLtnglqI35Mc
#beLLWA8wgacGA1UdIwSBnzCBnKGBjqSBizCBiDELMAkGA1UEBhMCUlUxDzANBgNV
#BAgMBk1vc2NvdzEPMA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVLT1JVUzELMAkG
#A1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJKoZIhvcNAQkB
#FhJzLnNhZmFyb3ZAbWFpbC5jb22CCQCPMHdgV/rQBjANBgkqhkiG9w0BAQUFAAOB
#gQAUSzkygTeqlY/b+EJkITJvEa2Owe+/IJPib2aA/PavrVyAb5gfKOoUh/lsXVks
#CkL9oe009UvFfF4vFkgnocW4DfNk0H/2Vfw2PDC/f+liI+ZPRXZDn66ntFrK55jz
#bgmRWPNLbzbjiPxIVJWnvli4l4ZcV37LxAry1qzDkBErAA==
#-—END CERTIFICATE——-

#Store certificate

pkcs15-tool --list-keys
#Using reader with a card: Aktiv Rutoken ECP 00 00
#Private RSA Key [My Private Key]
#	Object Flags   : [0x3], private, modifiable
#	Usage          : [0x4], sign
#	Access Flags   : [0x1D], sensitive, alwaysSensitive, neverExtract, local
#	ModLength      : 1024
#	Key ref        : 1 (0x1)
#	Native         : yes
#	Path           : 3f001000100060020001
#	Auth ID        : 02
#	ID             : 04830838b7c9752bd0e90c96a88b989a80f45181
#	GUID           : {04830838-b7c9-752b-d0e9-0c96a88b989a}
pkcs15-init --store-certificate tester.crt --auth-id 02 --id 04830838b7c9752bd0e90c96a88b989a80f45181 --format pem
pkcs15-tool --read-certificate 04830838b7c9752bd0e90c96a88b989a80f45181
#Using reader with a card: Aktiv Rutoken ECP 00 00
#-----BEGIN CERTIFICATE-----
#MIIDfjCCAuegAwIBAgIBATANBgkqhkiG9w0BAQUFADCBiDELMAkGA1UEBhMCUlUx
#DzANBgNVBAgMBk1vc2NvdzEPMA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVLT1JV
#UzELMAkGA1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJKoZI
#hvcNAQkBFhJzLnNhZmFyb3ZAbWFpbC5jb20wHhcNMTIxMTE0MDkwNDQ0WhcNMTMx
#MTE0MDkwNDQ0WjB3MQswCQYDVQQGEwJSVTEPMA0GA1UECAwGTW9zY293MQ4wDAYD
#VQQKDAVLT1JVUzELMAkGA1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92
#MSEwHwYJKoZIhvcNAQkBFhJzLnNhZmFyb3ZAbWFpbC5jb20wgZ8wDQYJKoZIhvcN
#AQEBBQADgY0AMIGJAoGBAKljQszR4EMoBOtHjt+gWfNWsxUSX6h4vHx98BQNEiwr
#IgsEMQ6uGPfAP/yo1IFeXQJNdEvWM8n8KaPLqyL4Am1UJue6F7Ys/TriRxuPXt66
#sC4CE/AoyUuGNyGw+gRyM3bZ9BgCJ75c35q1O1Zq4Vqc5Xq9qVUazJuhJxs/INt5
#AgMBAAGjggEGMIIBAjAJBgNVHRMEAjAAMCwGCWCGSAGG+EIBDQQfFh1PcGVuU1NM
#IEdlbmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQU3UfE21dLBLtnglqI35Mc
#beLLWA8wgacGA1UdIwSBnzCBnKGBjqSBizCBiDELMAkGA1UEBhMCUlUxDzANBgNV
#BAgMBk1vc2NvdzEPMA0GA1UEBwwGTW9zY293MQ4wDAYDVQQKDAVLT1JVUzELMAkG
#A1UECwwCSVQxFzAVBgNVBAMMDlNlcmdleSBTYWZhcm92MSEwHwYJKoZIhvcNAQkB
#FhJzLnNhZmFyb3ZAbWFpbC5jb22CCQCPMHdgV/rQBjANBgkqhkiG9w0BAQUFAAOB
#gQAUSzkygTeqlY/b+EJkITJvEa2Owe+/IJPib2aA/PavrVyAb5gfKOoUh/lsXVks
#CkL9oe009UvFfF4vFkgnocW4DfNk0H/2Vfw2PDC/f+liI+ZPRXZDn66ntFrK55jz
#bgmRWPNLbzbjiPxIVJWnvli4l4ZcV37LxAry1qzDkBErAA==
#-----END CERTIFICATE-----
#
#Speed

#(With OpenSC 0.14.0 pkcs15-init tool on Ubuntu 15.04)

#    Erasing: ~0m15s
#    GOST key generation: 0m3.883s
#    RSA 1024 key generation: ~0m30s
#    RSA 2048 key generation: ~6m00s
#
#Notes
#
#    When initialising with pkcs15-init, a PUK code must not be present (press enter when asked or use —puk "")
#    Card can be erased with pkcs15-init —erase-card (including all keys) without any authentication.

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
