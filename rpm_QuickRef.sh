# Install the package     rpm -ivh mozilla-mail-1.7.5-17.i586.rpm
rpm -ivh {rpm-file}     
rpm -ivh --test mozilla-mail-1.7.5-17.i586.rpm

#Upgrade package     rpm -Uvh mozilla-mail-1.7.6-12.i586.rpm
rpm -Uvh {rpm-file}     
rpm -Uvh --test mozilla-mail-1.7.6-12.i586.rpm

#Erase/remove/ an installed package  rpm -ev mozilla-mail
rpm -ev {package}   

#Erase/remove/ an installed package without checking for dependencies    rpm -ev --nodeps mozilla-mail
rpm -ev --nodeps {package}  

#Display list all installed packages     rpm -qa
rpm -qa     
rpm -qa | less

#Display installed information along with package version and short description  rpm -qi mozilla-mail
rpm -qi {package}   

#Find out what package a file belongs to i.e. find what package owns the file    rpm -qf /etc/passwd
rpm -qf {/path/to/file}     
rpm -qf /bin/bash

#Display list of configuration file(s) for a package     rpm -qc httpd
rpm -qc {pacakge-name}  

#Display list of configuration files for a command   rpm -qcf /usr/X11R6/bin/xeyes
rpm -qcf {/path/to/file}    

#Display list of all recently installed RPMs     rpm -qa --last
rpm -qa --last  
rpm -qa --last | less
rpm -qpR {.rpm-file}

#Find out what dependencies a rpm file has   rpm -qpR mediawiki-1.4rc1-4.i586.rpm
rpm -qR {package}   
rpm -qR bash


# Install an RPM Package
# -i : install a package
# -v : verbose for a nicer display
# -h: print hash marks as the package archive is unpacked.
rpm -ivh pidgin-2.7.9-5.el6.2.i686.rpm

# check dependencies of RPM Package before Installing
# -q : Query a package
# -p : List capabilities this package provides.
# -R: List capabilities on which this package depends..
rpm -qpR BitTorrent-5.2.2-1-Python2.4.noarch.rpm

# Install a RPM Package Without Dependencies
rpm -ivh --nodeps BitTorrent-5.2.2-1-Python2.4.noarch.rpm

# check an Installed RPM Package
rpm -q BitTorrent

# List all files of an installed RPM package
rpm -ql BitTorrent

# List Recently Installed RPM Packages
rpm -qa --last

# List All Installed RPM Packages
rpm -qa

# Upgrade a RPM Package
rpm -Uvh nx-3.5.0-2.el6.centos.i686.rpm

# Remove a RPM Package
rpm -evv nx

# Remove an RPM Package Without Dependencies
rpm -ev --nodeps vsftpd

# Query a file that belongs which RPM Package
rpm -qf /usr/bin/htpasswd

# Query a Information of Installed RPM Package
rpm -qi vsftpd

# List the Dependency Packages 
rpm -qRp MySQL-client-3.23.57-1.i386.rpm

# state of files in a package using rpm -qsp
rpm -qsp MySQL-client-3.23.57-1.i386.rpm

# Information of RPM Package Before Installing
rpm -qip sqlbuddy-1.3.3-1.noarch.rpm

# rpm -qip sqlbuddy-1.3.3-1.noarch.rpm
rpm -qdf /usr/bin/vmstat

# Verify a RPM Package
# S file Size differs
# M Mode differs (includes permissions and file type)
# 5 MD5 sum differs
# D Device major/minor number mismatch
# L readlink(2) path mismatch
# U User ownership differs
# G Group ownership differs
# T mTime differs

rpm -Vp sqlbuddy-1.3.3-1.noarch.rpm

# Verify all RPM Packages
rpm -Va

# Import an RPM GPG key
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

# List all Imported RPM GPG keys
rpm -qa gpg-pubkey*

# rebuild Corrupted RPM Database
cd /var/lib
rm __db*
rpm --rebuilddb
rpmdb_verify Packages





### INSTALLATION ###

# install
rpm -i ${packagename}.rpm
 
# install with progress
rpm -ivh ${packagename}.rpm
 
# install in full-on debug mode
rpm -ivvvh ${packagename}.rpm
 
# install without running scripts
rpm -i ${packagename}.rpm --noscripts
 
# install even if this version is already installed
rpm -i ${packagename}.rpm --force
 
### UNINSTALLATION ###
 
# uninstall
rpm -e ${packagename}
 
# uninstall without running scripts
rpm -e ${packagename} --noscripts
 
# uninstall even if there are dependencies
rpm -e ${packagename} --nodeps
 
# uninstall all rpms containing "foo" in name
rpm -e `rpm -qa | grep foo`
 
### UPGRADE ###
 
# upgrade
rpm -U ${packagename}.rpm
 
# install without running scripts
rpm -U ${packagename}.rpm --noscripts
 
# update even if this version is already installed
rpm -U ${packagename}.rpm --force
 
### ANALYSIS OF UNINSTALLED RPM FILES ###
 
# print properties
rpm -qipv ${packagename}.rpm
 
# print filelist
rpm -qlpv ${packagename}.rpm
 
# print required dependencies
rpm -qpR ${packagename}.rpm
 
### ANALYSIS OF INSTALLED RPM FILES
 
# list all installed rpms containing "foo" in name
rpm -qa | grep foo
 
# print installed files of installed package
rpm -ql ${packagename} 



# List all installed packages
rpm -q -a

# Upgrade packages
rpm -U -v *.rpm

# Freshen packages This is the one you should use when applying the latest fixes
rpm -Fvh *.rpm

# To find which rpm file (not installed) has the file libX
for i in `cat <dir to rpm files>`; do if rpm -qpl $i | grep libX >/dev/null; then echo $i; fi; done


# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
