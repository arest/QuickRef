#============== awk examples ===============
  awk -F"," 'NR!=1{print $1,$3}' OFS="," file1
#	-F","		- input field separator
#	-F,		
#	FS=","		 
#	
#	OFS="-"		- output field serparator
#	NR!=1		- denotes line number ranging from 1 to the actual line count. 
#			  The conditon 'NR!=1' indicates not to execute the action part 
#			  for the first line of the file, and hence the header record gets skipped.
#
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
