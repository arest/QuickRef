# How to find files containing a specific word in its name?
find /etc -name "*mail*"

# How to find all the files greater than certain size? 
find / -type f -size +100M

# How to find files that are not modified in the last x number of days? 
find . -mtime +60

# How to find files that are modified in the last x number of days?
find . –mtime -2

# How to delete all the archive files with extension *.tar.gz and greater than 100MB? 
find / -type f -name *.tar.gz -size +100M -exec ls -l {} \;
find / -type f -name *.tar.gz -size +100M -exec rm -f {} \;

# How to archive all the files that are not modified in the last x number of days?
find /home/jsmith -type f -mtime +60 | xargs tar -cvf /tmp/`date '+%d%m%Y'_archive.tar`

# Find files larger than 10MB in the current directory downwards…
find . -size +10000000c -ls

# Find files larger than 100MB…
find . -size +100000000c -ls

#Find files last modified over 30days ago…
find . -type f -mtime 30 -ls

#Find files last modified over 365days ago…
find . -type f -mtime 365 -ls

#Find files last accessed over 30days ago…
find . -type f -atime 30 -ls

#Find files last accessed over 365days ago…
find . -type f -atime 365 -ls

#If the file is being updated at the current time then we can use find to find files modified in the last day…
find  . -type f -mtime -1 -ls

#Better still, if we know a file is being written to now, we can touch a file and ask the find command to list any files updated after the timestamp of that file, which will logically then list the rogue file in question.
touch testfile
find .  -type f -newer testfile -ls

#A clean up of redundant tar (backup) files, after completing a piece of work say, is sometimes forgotten. Conversely, if tar files are needed, they can be identified and duly compressed (using compress or gzip) if not already done so, to help save space. Either way, the following lists all tar files for review.
find . -type f -name "*.tar" -ls
find . -type f -name "*.tar.Z" -ls

#List, in order, the largest sub-directories (units are in Kb)…
du -sk * | sort -n

#Sometimes it is useful to then cd into that suspect directory and re-run the du command until the large files are found.

#The above find commands can be edited to remove the files found rather than list them. The “-ls” switch can be changed for “-exec rm {}\;”=.
find . -type f -mtime 365 -exec rm {} \;

#Find files which has read permission only to group.
find . -perm g=r -type f -exec ls -l {} \;

#List all the empty files only in your home directory.
find . -maxdepth 1 -empty

#List only the non-hidden empty files only in the current directory.
find . -maxdepth 1 -empty -not -name ".*"

#The following command will display the top 5 largest file in the current directory and its subdirectory. This may take a while to execute depending on the total number of files the command has to process.
find . -type f -exec ls -s {} \; | sort -n -r | head -5

#Technique is same as finding the bigger files, but the only difference the sort is ascending order.
find . -type f -exec ls -s {} \; | sort -n  | head -5

#List the smaller files other than the ZERO byte files.
find . -not -empty -type f -exec ls -s {} \; | sort -n  | head -5

#Find files bigger than the given size
find ~ -size +100M

#Find files smaller than the given size
find ~ -size -100M

#Find files that matches the exact given size
find ~ -size 100M

#Find files whose content got updated within last 1 hour
#To find the files based up on the content modification time, the option -mmin, and -mtime is used. Following is the definition of mmin and mtime from man page.
# -mmin n File’s data was last modified n minutes ago.
# -mtime n File’s data was last modified n*24 hours ago.
#Following example will find files in the current directory and sub-directories, whose content got updated within last 1 hour (60 minutes)
find . -mmin -60

# finds all the files (under root file system /) that got updated within the last 24 hours (1 day).
find / -mtime -1

#Find files which got accessed before 1 hour
#To find the files based up on the file access time, the option -amin, and -atime is used. Following is the definition of amin and atime from find man page.
#    -amin n File was last accessed n minutes ago
#    -atime n File was last accessed n*24 hours ago
#Following example will find files in the current directory and sub-directories, which got accessed within last 1 hour (60 minutes)
find -amin -60

#Find files which got changed exactly before 1 hour
#To find the files based up on the file inode change time, the option -cmin, and -ctime is used. Following is the definition of cmin and ctime from find man page.
#    -cmin n File’s status was last changed n minutes ago.
#    -ctime n File’s status was last changed n*24 hours ago.
#Following example will find files in the current directory and sub-directories, which changed within last 1 hour (60 minutes)
find . -cmin -60
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
