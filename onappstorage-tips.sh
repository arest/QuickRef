onappstore diskinfo uuid=keaunl3tdbmo4r| sed "s/ /\n/g" | sed -n "/^memberinfo=[{]/{s/^memberinfo=[{]//;s/[}][[:space:]]*\$//;s/\([}],\)/\\1\n/g;p}"


onappstore nodeinfo uuid=2973395356| sed -n "{s/ /\n/g;p}"  | sed -n "/^ipaddr=/{p}"


onappstore forget forgetlist=2595298778 vdi_uuid=e8l60ovtcq1im2



# list of snapshots:

onappstore list | sed -n "\${H;x;s/\n/ /g;s/ \+/ /g;s/| Node /\nNode /;p;d;};/^Node /{s/\$/|/;x;s/\n/ /g;s/ \+/ /g;s/|\$//;p;d;};{s/\$/|/;H;d}"| sed -n  "/Snapshot: 1/{s/|/\n    /g;p}"


# uuid list of vdisks

 onappstore list | sed -n "/^Node [[][[:alnum:]]\{14\}[]]/{s/^Node [[]\([[:alnum:]]\{14\}\)[]].*\$/\1/;p}"


# list device in device-mapper that are not in active state:
for DMPATH in $(dmsetup ls | awk "{print \$1}"); do DMSTATE=$(dmsetup info $DMPATH | awk "/State:/{print \$2}"); if [ "$DMSTATE" != "ACTIVE" ] ; then echo "$DMPATH - $DMSTATE"; fi; done

for HVIP in 10.35.34.80 10.35.34.71 10.35.34.72 10.35.34.73 ; do echo $HVIP; \
ssh $HVIP "for DMPATH in \$(dmsetup ls | awk \"{print \\\$1}\"); do DMSTATE=\$(dmsetup info \$DMPATH | awk \"/State:/{print \\\$2}\"); if [ \"\$DMSTATE\" == \"ACTIVE\" ] ; then echo \"\$DMPATH - \$DMSTATE\"; fi; done";\
 echo ; done

# Free space on nodes for vDisk:
# free space by onappstore nodeinfo is reported in blocks by 512 bytes

for NODE in $(onappstore diskinfo uuid=oviham45ewcx0b | sed "s/ /\n/g"| sed -n "/^members=/{s/^members=//;s/,/\n/g;p}"); do  FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024  / 1024 ); echo "Node: $NODE   free space: $FS GB"; done

__improved:

for NODE in $(onappstore diskinfo uuid=xk5u7o4zapisdq | sed "s/ /\n/g"| sed -n "/^members=/{s/^members=//;s/,/\n/g;p}"); do NODEIP=$(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^ipaddr=/{s/^ipaddr=//;p}"); FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024  / 1024 );  VDISK_NUMBER=$(onappstore nodeinfo uuid=$NODE| sed "s/ /\n/g" | sed -n "/^vdisks=/{s/^vdisks=//;s/,/\n/g;p}"| grep -v "^[[:space:]]*\$"|wc -l); echo "Node: $NODE  (data store uuid=<$DATASTORE>, node_ipaddr=$NODEIP )  free space: $FS GB, hosts $VDISK_NUMBER vdisks"; done
___________

Free space that can be allocated on each backend node:

for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do  FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024  / 1024 );DATASTORE=$(onappstore nodeinfo uuid=$NODE |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); echo "Node: $NODE  (data store uuid=<$DATASTORE>)  free space: $FS GB"; done


# free space with number hosted vdisks and ip address  with filtering by free space
---------------------------------------
for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do NODEIP=$(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^ipaddr=/{s/^ipaddr=//;p}"); FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024  / 1024 );DATASTORE=$(onappstore nodeinfo uuid=$NODE |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); VDISK_NUMBER=$(onappstore nodeinfo uuid=$NODE| sed "s/ /\n/g" | sed -n "/^vdisks=/{s/^vdisks=//;s/,/\n/g;p}"| grep -v "^[[:space:]]*\$"|wc -l); if [ $FS -ge 0 ] ;then echo "Node: $NODE  (data store uuid=<$DATASTORE>, node_ipaddr=$NODEIP )  free space: $FS GB, hosts $VDISK_NUMBER vdisks"; fi; done
---------------------------------------


# free space, allocated space, node size number hosted vdisks
---------------------------------------
for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do A_SP=$(expr $(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^allocated_space=/{s/^allocated_space=//;p}") / 1024 / 1024 / 1024);N_SZ=$(expr $(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^size_bytes=/{s/^size_bytes=//;p}") / 1024 / 1024 / 1024);NODEIP=$(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^ipaddr=/{s/^ipaddr=//;p}"); FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024  / 1024 );DATASTORE=$(onappstore nodeinfo uuid=$NODE |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); VDISK_NUMBER=$(onappstore nodeinfo uuid=$NODE| sed "s/ /\n/g" | sed -n "/^vdisks=/{s/^vdisks=//;s/,/\n/g;p}"| grep -v "^[[:space:]]*\$"|wc -l); if [ $FS -ge 0 ] ;then echo "Node: $NODE  (data store uuid=<$DATASTORE>, node_ipaddr=$NODEIP )  free space: $FS GB, allocated space: $A_SP GB, node size: $N_SZ GB, hosts $VDISK_NUMBER vdisks"; fi; done
---------------------------------------

=====================================================
#free space, allocated space,nodes size, difference , conditional output
=====================================================
for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do A_SP=$(expr $(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^allocated_space=/{s/^allocated_space=//;p}") / 1024 / 1024 / 1024);N_SZ=$(expr $(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^size_bytes=/{s/^size_bytes=//;p}") / 1024 / 1024 / 1024);NODEIP=$(onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g"| sed -n "/^ipaddr=/{s/^ipaddr=//;p}"); FS=$(expr $( onappstore nodeinfo uuid=$NODE | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024 / 1024 );DATASTORE=$(onappstore nodeinfo uuid=$NODE |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); VDISK_NUMBER=$(onappstore nodeinfo uuid=$NODE| sed "s/ /\n/g" | sed -n "/^vdisks=/{s/^vdisks=//;s/,/\n/g;p}"| grep -v "^[[:space:]]*\$"|wc -l); DIFF=$(expr $N_SZ - $A_SP - $FS); DIFF=$(echo $DIFF| sed "s/-//g"); if [ $DIFF -ge 0 ] ;then echo "Node: $NODE (data store uuid=<$DATASTORE>, node_ipaddr=$NODEIP ) free space: $FS GB, allocated space: $A_SP GB, node size: $N_SZ GB, hosts $VDISK_NUMBER vdisks; following space is not taken into account: $DIFF GB"; fi; done

=====================================================

for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do NODEINFO=$(onappstore nodeinfo uuid=$NODE); UTILIZATION=$(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^utilization=/{s/^utilization=//;s/[[:space:]]*//g;p}");IPADDR=$(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^ipaddr=/{s/^ipaddr=//;s/[[:space:]]*//g;p}"); NODESIZE=$( expr $(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^size_bytes=/{s/^size_bytes=//;s/[[:space:]]*//g;p}") / 1024 / 1024 / 1024 ); FREESPACE=$( expr $(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024 / 1024 ); ALLOCATEDSPACE=$(expr $(echo -e "${NODEINFO}"| sed "s/ /\n/g"| sed -n "/^allocated_space=/{s/^allocated_space=//;p}") / 1024 / 1024 / 1024); DATASTORE=$(echo -e "${NODEINFO}" |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); DIFF=$(expr $NODESIZE - $ALLOCATEDSPACE - $FREESPACE); DIFF=$(echo $DIFF| sed "s/-//g");if [ "${DIFF}" -gt 5 ] ; then printf "%14s %16s total: %5s GB free: %5s GB allocated: %5s GB usage:%3s%% %s\n" "${NODE}" "$IPADDR" "$NODESIZE" "${FREESPACE}" "${ALLOCATEDSPACE}" "${UTILIZATION}" "${DIFF}" ; fi; done

=====================================================

for NODE in $(onappstore nodes| sed -n "/^Node: /{s/^Node: //;s/ //g;p}"); do \
NODEINFO=$(onappstore nodeinfo uuid=$NODE); \
UTILIZATION=$(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^utilization=/{s/^utilization=//;s/[[:space:]]*//g;p}");\
IPADDR=$(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^ipaddr=/{s/^ipaddr=//;s/[[:space:]]*//g;p}"); \
NODESIZE=$( expr $(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^size_bytes=/{s/^size_bytes=//;s/[[:space:]]*//g;p}") / 1024 / 1024 / 1024 );\
FREESPACE=$( expr $(echo -e "${NODEINFO}" | sed "s/ /\n/g" | sed -n "/^freespace=/{s/^freespace=//;s/ //g;p}") \* 512 / 1024 / 1024 / 1024 );\
ALLOCATEDSPACE=$(expr $(echo -e "${NODEINFO}"| sed "s/ /\n/g"| sed -n "/^allocated_space=/{s/^allocated_space=//;p}") / 1024 / 1024 / 1024);\
DATASTORE=$(echo -e "${NODEINFO}" |sed "s/ /\n/g"| sed -n "/^datastoremember=/{s/^datastoremember=//;s/[[:space:]]*\$//;p}"); \
DIFF=$(expr $NODESIZE - $ALLOCATEDSPACE - $FREESPACE); \
DIFF=$(echo $DIFF| sed "s/-//g");\
\
if [ "${DIFF}" -gt 5 ] ; \
then \
printf "%14s ds_uuid=%14s %15s total: %5s GB free: %5s GB allocated: %5s GB usage:%3s%% lost space: %5s GB\n" \
"${NODE}" "${DATASTORE}" "${IPADDR}" "${NODESIZE}" "${FREESPACE}" "${ALLOCATEDSPACE}" "${UTILIZATION}" "${DIFF}" ; \
fi; \
done


=====================================================



##Get membership from output of onappstore diskfino uuid=

onappstore diskinfo uuid=1hm35knw2czfog | sed -n "/^.*membership=[[]/{s/^.*\(membership=[[][^]]\+[]]\).*\$/\1/;p}"


====================================

##
## not existe backend node in memebers but existed in metadata (membership)
##

VDISKUUID=1hm35knw2czfog ;MEMBERS=$(onappstore diskinfo uuid=$VDISKUUID | sed -n "/^.*members=/{s/^.*members=\([^[:space:]]*\).*\$/\1/;s/,/\n/g;p}"); MEMBERSHIP=$(onappstore diskinfo uuid=$VDISKUUID | sed -n "/^.*membership=[[]/{s/^.*membership=[[]\([^]]\+\)[]].*\$/\1/;s/[[:space:]]*//g;s/u'//g;s/'//g;s/,/\n/g;p}"); for BNODE in $(echo -e $MEMBERSHIP); do if  ! echo -e "$MEMBERS" | grep -q $BNODE ; then echo "$BNODE is not present in members, probably should be forgotten for vdisk $VDISKUUID"; fi; done


====================================

#
# list vdisks which contains backend node uuid=1991818513
#

for VDISKUUID in $(onappstore list | sed -n "/^Node [[][[:alnum:]]\{14\}[]]/{s/^Node [[]\([[:alnum:]]\{14\}\)[]].*\$/\1/;p}") ; do VDISKMEMBERSHIP=$(onappstore diskinfo uuid=$VDISKUUID | sed -n "/^.*membership=[[]/{s/^.*membership=[[]\([^]]\+\)[]].*\$/\1/;s/[[:space:]]*//g;s/u'//g;s/'//g;s/,/\n/g;p}"); if echo -e $VDISKMEMBERSHIP | grep -q 1991818513 ; then echo $VDISKUUID ; fi ; done

=====================================

#
# List of degraded vdisks
#

/usr/pythoncontroller/getdegradedvdisks | sed -n "/^[[:space:]]*Degraded Source VDisks:/{s/^[[:space:]]*Degraded Source VDisks:[[:space:]]*[[:digit:]]*[[:space:]]*[(]\([^)]*\)[)].*\$/\1/;s/,/\n/g;p}"


=====================================

# get size of vdisk in bytes:

onappstore diskinfo uuid=rdcaue1mpqw0y6| sed "{s/ /\n/g};{s/.*\n\(size=\([[:digit:]]\+\)\)\n.*/\2/;}"

# get list of vdisks hosted on some backend node

onappstore nodeinfo uuid=2718226208| sed "{s/[[:space:]]/\n/g}"| sed -n "/^vdisks=/{s/^vdisks=//;s/,/\n/g;p}"


mysql -p onapp -e "select id,label,ip_address,concat('10.200.',host_id,'.254') as SAN_ip, host_id from hypervisors;"



backup with lvm 
=================================

OK, so looking at this HV, the device refcount directory was not initialized at all:

[root@10.0.0.105 ~]# getfreedevices vdisk=j6ng5kb7i4mn4q 
FAILURE Error='NoneType' object is not iterable

[root@10.0.0.105 ~]# ls -al /tmp/NBDdevs 
ls: cannot access /tmp/NBDdevs: No such file or directory

Usually this is done when the storage service (groupmon) starts up, but since this customer is not using IS, they dont have the storage service running:

[root@10.0.0.105 ~]# ps ax | grep group 
23 ? S 0:00 [cgroup] 
31216 pts/0 S+ 0:00 grep group

To fix this temporarily I checked which nbd devices are actually available on the system:

[root@10.0.0.105 ~]# ls -al /dev/nbd* | wc -l 
512 
[root@10.0.0.105 ~]#

Then I checked if any of them are being actively used at the moment:

[root@10.0.0.105 ~]# ps ax |grep nbd 
32621 pts/0 S+ 0:00 grep nbd 
[root@10.0.0.105 ~]#

I then initialized the refcount directory by creating 512 devices (as no devices are being used currently) in it as follows:

[root@10.0.0.105 ~]# mkdir /tmp/NBDdevs 
[root@10.0.0.105 ~]# mkdir /tmp/NBDdevs/freedevs 
[root@10.0.0.105 ~]# mkdir /tmp/NBDdevs/inusedevs 
[root@10.0.0.105 ~]# for i in `seq 0 511`; do touch /tmp/NBDdevs/freedevs/$i; done 
[root@10.0.0.105 ~]# ls -al /tmp/NBDdevs/freedevs/ | wc -l 
515

I then tried the getfreedevices command which worked:

[root@10.0.0.105 ~]# getfreedevices vdisk=j6ng5kb7i4mn4q 
SUCCESS devices=nbd99

Followed by a putdevices to return the devices to the free list:

[root@10.0.0.105 ~]# putdevices 
Usage: putdevices vdisk=<VDISK_UUID> devices=COMMA_SEPARATED_LIST_OF_NBD_DEVICES 
[root@10.0.0.105 ~]# putdevices vdisk=j6ng5kb7i4mn4q devices=nbd99 
SUCCESS 
[root@10.0.0.105 ~]# ls -al /tmp/NBDdevs/freedevs/ | wc -l 
515 
[root@10.0.0.105 ~]#

Now I think we should get some input from Igor on how the the refcount directory is supposed to be initialized in the first place for non-IS hosts. Please record these steps for any further issues related to this.

Assigning to L3 to inform the customer that we have unblocked them for the moment and then assign to Igor to comment when he is back on Monday.


=================================



OnApp version onapp-store 3.0.10 :
[root@Management01 ~]# md5sum /tftpboot/export/centos5/xen/lib/modules/*/kernel/drivers/xen/blkback/blkbk.ko
926776a6fab1b3ba74fed9501989df77  /tftpboot/export/centos5/xen/lib/modules/2.6.18-308.20.1.el5.onappxen/kernel/drivers/xen/blkback/blkbk.ko
2d9a2e86843ae603e9c988e16a020ac5  /tftpboot/export/centos5/xen/lib/modules/2.6.18-348.4.1.el5.onappxen/kernel/drivers/xen/blkback/blkbk.ko
[root@Management01 ~]#

============================================================================
# check if stripes members are spreaded properly over HVs (host_id)
============================================================================

VDISK=g5iwtpnzx2fqcv ; VDISKINFO=$(onappstore diskinfo uuid=$VDISK); NODESINFO=$( echo -e "${VDISKINFO}" | sed "s/ /\n/g" | sed -n "/^memberinfo=[{]/{s/^memberinfo=[{]//;s/[}][[:space:]]*\$//;s/\([}],\)/\\1\n/g;p}" ); for NODE in $(echo -e "$NODESINFO"|sed "s/^u'\([[:digit:]]\+\)'.*/\1/") ; do STMEM=$(echo -e "$NODESINFO"| sed -n "/^u'${NODE}'.*'st_mem':[0123],.*\$/{s/^u'${NODE}'.*'st_mem':\([0123]\),.*\$/\1/;p}"); HOSTID=$(onappstore nodeinfo uuid=$NODE|sed "s/ /\n/g" | sed -n "/^hostid=/{s/^hostid=\([[:digit:]]\+\)[[:space:]]*\$/\1/;p}");printf "st_mem=%s host_id=%s node=%s\n" "${STMEM}" "${HOSTID}" ${NODE}; done | sort


============================================================================
spreading stripe members of all vdisks over host_id and backend nodes
============================================================================

for VDISK in $(onappstore list | sed -n "/^Node[[:space:]]*[[][[:alnum:]]\{14\}[]][[:space:]]*\$/{s/^Node[[:space:]]*[[]\([[:alnum:]]\{14\}\)[]][[:space:]]*\$/\1/;p}"); do echo "vdisk uuid=${VDISK}";VDISKINFO=$(onappstore diskinfo uuid=$VDISK); NODESINFO=$( echo -e "${VDISKINFO}" | sed "s/ /\n/g" | sed -n "/^memberinfo=[{]/{s/^memberinfo=[{]//;s/[}][[:space:]]*\$//;s/\([}],\)/\\1\n/g;p}" ); for NODE in $(echo -e "$NODESINFO"|sed "s/^u'\([[:digit:]]\+\)'.*/\1/") ; do STMEM=$(echo -e "$NODESINFO"| sed -n "/^u'${NODE}'.*'st_mem':[0123],.*\$/{s/^u'${NODE}'.*'st_mem':\([0123]\),.*\$/\1/;p}"); HOSTID=$(onappstore nodeinfo uuid=$NODE|sed "s/ /\n/g" | sed -n "/^hostid=/{s/^hostid=\([[:digit:]]\+\)[[:space:]]*\$/\1/;p}");printf "st_mem=%s host_id=%s node=%s\n" "${STMEM}" "${HOSTID}" ${NODE}; done | sort; echo ; done

============================================================================


============================================================================
# list of disk drives with their signatures on HV
============================================================================
for DRIVE in $(fdisk -l | sed -n "/^Disk[[:space:]]\+\/dev\/sd[a-z]\+:[[:space:]]\+.*\$/{s/^Disk[[:space:]]\+\(\/dev\/sd[a-z]\+\):[[:space:]]\+.*\$/\1/;p}"); do DRIVEINFO=$(onapp_scsi_id ${DRIVE}); ID_SERIAL=$(echo -e "${DRIVEINFO}" | sed -n "/^ID_SERIAL=/{s/^ID_SERIAL=//;s/[[:space:]]//g;p}"); ID_SCSI_SERIAL=$(echo -e "${DRIVEINFO}" | sed -n "/^ID_SCSI_SERIAL=/{s/^ID_SCSI_SERIAL=//;s/[[:space:]]//g;p}"); SIGNATURE="${ID_SERIAL}_${ID_SCSI_SERIAL}"; printf "%-45s %s\n" "${SIGNATURE}" "${DRIVE}";done | sort

============================================================================

============================================================================
### memory allocation fix for storage controller VMs 
sed -i "/^ramperctlr=640[[:space:]]*\$/{s/^ramperctlr=640[[:space:]]*\$/ramperctlr=2048/}" /.rw/onappstore.conf 
/etc/init.d/groupmon restart 
####

#####
#delete old stats from /tmp/NBDstats if its size is over 128Mb
#####
echo "*/15 *  *  *  * root       bash -c '" 'while [ "1" == "1" ] ; do cd /tmp/NBDstats || exit 0; DELFILE=$(ls -t1 /tmp/NBDstats/ | tail -n 1); if [ ! -e "$DELFILE" ] ; then exit 0; fi; DU=$(du -ms /tmp/NBDstats/ | sed -n "/^[[:space:]]*[[:digit:]]\+[[:space:]].*\$/{s/^[[:space:]]*\([[:digit:]]\+\)[[:space:]].*$/\1/;p}"); if [ $DU -lt 128 ] ; then exit 0; fi; rm "$DELFILE"; done' "'" >> /etc/crontab 
#####


VirtualMachine.all.each { |vm| vm.preferred_hvs = vm.preferred_hvs - [4]; vm.save }

### list of vdisks whit incync status not equal 1
### list of vdisks that should be repaired

for VDISK in $(onappstore list | sed -n "/^Node[[:space:]]*[[][^]]*[]][[:space:]]*\$/{s/^Node[[:space:]]*[[]\([^]]*\)[]][[:space:]]*\$/\1/;p}"); do DISKINFO=$(onappstore diskinfo uuid=${VDISK}); SYNCSTATUS=$(echo -e "${DISKINFO}" |  sed "s/ /\n/g" | sed -n "/^insyncstatus=/{s/^insyncstatus=//;p}");if [ $SYNCSTATUS -eq 1 ] ; then continue ; fi ; echo "$VDISK"; done



for VMCIP in $(onappstore nodes | sed -n "/IP addr: 10.200/{s/^[[:space:]]*IP addr:[[:space:]]*//;p}" |grep -v "10\.200\.[[:digit:]]\{1,3\}\.254"| sort | uniq); do echo $VMCIP;(echo "ps w | grep storageAPI";sleep 5)|telnet $VMCIP; echo;echo;echo;done

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
