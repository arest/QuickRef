
git init
echo "First commit." > README.md
git add .
git commit -a
git push origin


git status


git add index.php 
git diff --cached 
git rm readme.txt 
git rm --cached readme.txt 
git reset HEAD readme.txt
git log 
gitk

git clone git@bitbucket.org:orest_pazdriy/quickref.git
git pull
git push
git remote -v



git commit -m 'some entry script fixes'
git commit -a -m 'some entry script fixes'


# Config/Init
git config
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
git config --global core.editor vim
git config --global merge.tool vimdiff
git config --list


git init project-name							# Создать новый репозиторий
git clone git@bitbucket.org:orest_pazdriy/quickref.git			# Клонировать репозиторий с удаленной машины
git add text.txt							# Добавить файл в репозиторий
git rm text.txt								# Удалить файл
git status								# Текущее состояние репозитория (изменения, неразрешенные конфликты и тп)
git commit -a -m "Commit description"					# Сделать коммит
git commit -a								# Сделать коммит, введя его описание с помощью $EDITOR
git push origin								# Замерджить все ветки локального репозитория на удаленный репозиторий
git push origin master							# Аналогично предыдущему, но делается пуш только ветки master
git push origin HEAD							# Запушить текущую ветку, не вводя целиком ее название
git pull origin								# Замерджить все ветки с удаленного репозитория
git pull origin master							# Аналогично предыдущему, но накатывается только ветка master
git pull origin HEAD							# Накатить текущую ветку, не вводя ее длинное имя
git fetch origin							# Скачать все ветки с origin, но не мерджить их в локальный репозиторий
git fetch origin master							# Аналогично предыдущему, но только для одной заданной ветки
git checkout -b some_branch origin/some_branch				# Начать работать с веткой some_branch (уже существующей)
git branch some_branch							# Создать новый бранч (ответвится от текущего)
git checkout some_branch						# Переключиться на другую ветку (из тех, с которыми уже работаем)
git branch 								# Получаем список веток, с которыми работаем. Звездочкой отмечена текущая ветвь
git branch -a								# Просмотреть все существующие ветви
git merge some_branch							# Замерджить some_branch в текущую ветку
git branch -d some_branch						# Удалить бранч (после мерджа)
git branch -D some_branch						# Просто удалить бранч (тупиковая ветвь)
git log									# Последние изменения
git log file.txt							# История конкретного файла
git log -p file.txt							# Аналогично предыдущему, но с просмотром сделанных изменений
git log --stat --graph							# История с именами файлов и псевдографическим изображением бранчей
git show d8578edf8458ce06fbc5bb76a58c5ca4a58c5ca4			# Изменения, сделанные в заданном коммите
git blame file.txt							# Посмотреть, кем в последний раз правилась каждая строка файла
git push origin :branch-name						# Удалить бранч из репозитория на сервере
git reset --hard d8578edf8458ce06fbc5bb76a58c5ca4a58c5ca4		# Откатиться к конкретному коммиту (хэш смотрим в «git log»)
git reset --soft d8578edf8458ce06fbc5bb76a58c5ca4a58c5ca4		# Аналогично предыдущему, но файлы на диске остаются без изменений
git revert d8578edf8458ce06fbc5bb76a58c5ca4a58c5ca4			# Попытаться обратить заданный commit (но чаще используется branch/reset + merge)
git diff 								# Просмотр изменений (суммарных, а не всех по очереди, как в «git log»), подробности см в "git diff --help"
git config --global merge.tool vimdiff					# Используем vimdiff в качестве программы для разрешения конфликтов (mergetool) по умолчанию
git config --global mergetool.prompt false				# Отключаем диалог «какой mergetool вы хотели бы использовать»
git mergetool								# Разрешение конфликтов (когда оные возникают в результате мерджа)
git tag some_tag 							# Создание тэга, за тэгом можно указать хэш коммита
git clean -f								# Удаление untracked files
git gc									# «Упаковка» репозитория для увеличения скорости работы с ним

# Следует отметить, что Git позволяет использовать короткую запись хэшей. Вместо «d8578edf8458ce06fbc5bb76a58c5ca4a58c5ca4» можно писать «d8578edf» или даже «d857»
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
