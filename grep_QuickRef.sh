egrep '^[^#\s]' 					# exclude whitespaces and comments
grep -v -e "^$" -e"^ *#"				# shows only lines that are not blank or commented out
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
