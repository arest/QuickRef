# 6.9 Revoke certificate
openssl ca -config etc/component-ca.conf -revoke \
	ca/component-ca/02.pem \
    -crl_reason superseded

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
