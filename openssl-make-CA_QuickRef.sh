DIR=/etc/ssl
ROOTCA=$DIR/root-ca
ROOTCONF=$ROOTCA/root-ca.conf
mkdir $ROOTCA
cd $ROOTCA
mkdir certs db private csr
chmod 700 private
touch $ROOTCA/db/index
openssl rand -hex 16 > $ROOTCA/db/serial
echo 1001 > $ROOTCA/db/crlnumber
cd -
cp root-ca.conf $ROOTCA

# 3. Root CA Generation
openssl req -new -config $ROOTCONF -out $ROOTCA/csr/ArestRootCA.csr -keyout $ROOTCA/private/ArestRootCA.key

openssl ca -selfsign -config $ROOTCONF -in $ROOTCA/csr/ArestRootCA.csr -out $ROOTCA/ArestRootCA.crt -extensions ca_ext

# To generate a CRL from the new CA
openssl ca -gencrl -config $ROOTCONF -out $ROOTCA/ArestRootCA.crl


openssl ca -config $ROOTCONF -revoke $ROOTCA/certs/1002.pem -crl_reason keyCompromise


# Create a Certificate for OCSP Signing
openssl req -new -newkey rsa:2048 -subj "/C=UA/O=Arest Inc./CN=OCSP Root Responder" -keyout $ROOTCA/private/ArestRootCA-OCSP.key -out $ROOTCA/csr/ArestRootCA-OCSP.csr

openssl ca -config $ROOTCONF -in $ROOTCA/csr/ArestRootCA-OCSP.csr -out $ROOTCA/ArestRootCA-OCSP.crt -extensions ocsp_ext -days 365

# Now you have everything ready to start the OCSP responder. For testing, you can do it from
# the same machine on which the root CA resides. However, for production you must move the
# OCSP responder key and certificate elsewhere:

openssl ocsp -port 9080 -index $ROOTCA/db/index -rsigner $ROOTCA/ArestRootCA-OCSP.crt -rkey $ROOTCA/private/ArestRootCA-OCSP.key -CA $ROOTCA/ArestRootCA.crt -text

# You can test the operation of the OCSP responder using the following command line:
openssl ocsp -issuer $ROOTCA/ArestRootCA.crt -CAfile $ROOTCA/ArestRootCA.crt -cert $ROOTCA/ArestRootCA-OCSP.crt -url http://127.0.0.1:9080


# Subordinate CA Generation
SUBCA=$DIR/sub-ca
SUBCONF=$SUBCA/sub-ca.conf
mkdir $SUBCA
cd $SUBCA
mkdir certs db private csr
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber

openssl req -new -config $SUBCONF -out $SUBCA/csr/ArestSubCA.csr -keyout $SUBCA/private/ArestSubCA.key

cd ../$ROOTCA

openssl ca -config $ROOTCONF -in $SUBCA/csr/ArestSubCA.csr -out $SUBCA/ArestSubCA.crt -extensions sub_ca_ext

cd $SUBCA

# To issue a server certificate


openssl req -new -newkey rsa:2048 -nodes -keyout $SUBCA/private/arest-home.pp.ua.key -reqexts server_ext -out $SUBCA/arest-home.pp.ua.csr

openssl ca -config $SUBCONF -in $SUBCA/csr/arest-home.pp.ua.csr -out $SUBCA/arest-home.pp.ua.crt -extensions server_ext

# To issue a client certificate


openssl req -new -newkey rsa:2048 -nodes -keyout $SUBCA/private/dr.arest@gmail.com.key -out $SUBCA/csr/Orest_Pazdriy.csr

openssl ca -config $SUBCONF -in $SUBCA/csr/Orest_Pazdriy.csr -out $SUBCA/Orest_Pazdriy.crt -extensions megaclient_ext


openssl x509 -in $ROOTCA/ArestRootCA.crt -out $ROOTCA/ArestRootCA.crt
openssl x509 -outform DER < $ROOTCA/ArestRootCA.crt > $ROOTCA/ArestRootCA.cer

openssl x509 -trustout < $ROOTCA/ArestRootCA.crt > $ROOTCA/ArestRootCA.trusted.crt
openssl x509 -outform DER < $ROOTCA/ArestRootCA.trusted.crt > $ROOTCA/ArestRootCA.trusted.cer

openssl rsa -pubout < private/ArestRootCA.key > ArestRootCA.pub
openssl rsa -pubin -in ArestRootCA.pub -pubout -outform DER -out ArestRootCA.pub.cer
openssl rsa -RSAPublicKey_out < private/ArestiRootCA.key > ArestRootCA.rsa.pub

pkcs11-tool -y cert -w ArestRootCA.trusted.cer --label ArestRootCA --id 1 --attr-from ArestRootCA.trusted.cer --pin 3128443
pkcs11-tool -y privkey -w private/ArestRootCA.key.cer --label ArestRootCA --id 1 --attr-from ArestRootCA.trusted.cer --pin 3128443 -l
pkcs11-tool -y pubkey -w ArestRootCA.pub.cer --label ArestRootCA --id 1 --attr-from ArestRootCA.trusted.cer --pin 3128443


openssl x509 -in ArestSubCA.crt -out ArestSubCA.crt
openssl x509 -outform DER < ArestSubCA.crt > ArestSubCA.cer

openssl x509 -trustout < ArestSubCA.crt > ArestSubCA.trusted.crt
openssl x509 -outform DER < ArestSubCA.trusted.crt > ArestSubCA.trusted.cer

openssl rsa -pubout < private/ArestSubCA.key > ArestSubCA.pub
openssl rsa -pubin -in ArestSubCA.pub -pubout -outform DER -out ArestSubCA.pub.cer
openssl rsa -RSAPublicKey_out < private/ArestSubCA.key > ArestSubCA.rsa.pub

openssl rsa -in private/ArestSubCA.key -out private/ArestSubCA.key.cer -outform DER
openssl x509 -in ArestSubCA.crt -out ArestSubCA.cer -outform DER
#ssh-keygen -i -f ArestSubCA.pub -m PKCS8 > ArestSubCA.ssh.pub

pkcs11-tool -y cert -w ArestSubCA.trusted.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
pkcs11-tool -y privkey -w private/ArestSubCA.key.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
pkcs11-tool -y pubkey -w ArestSubCA.pub.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
