# 6.1 Create TLS server request
export SRV=${1:=arest-home.pp.ua}
IP=${2:=193.93.219.55}
export SAN=DNS:${SRV},DNS:*.${SRV},IP:${IP} 
echo "SAN=$SAN"
openssl req -new -config etc/server.conf -out certs/${SRV}.csr -keyout private/${SRV}.key && \
# DN: C=SE, O=Arest Inc., CN=www.arest-home.pp.ua

# 6.2 Create TLS server certificate
openssl ca -config etc/component-ca.conf -engine pkcs11 -keyform e -keyfile slot_0-id_57e96a5de691d645 -in certs/${SRV}.csr -out certs/${SRV}.crt -extensions server_ext  
#openssl pkcs12 -export -in certs/${SRV}.crt -keyin private/${SRV}.key -out private/${SRV}.p12
cat certs/${SRV}.crt > certs/${SRV}-chain.crt
cat ca/component-ca.crt >> certs/${SRV}-chain.crt
cat ca/network-ca.crt >> certs/${SRV}-chain.crt
cat ca/root-ca.crt >> certs/${SRV}-chain.crt
openssl x509 -in certs/${SRV}.crt -out certs/${SRV}.cer -outform DER
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
