mkdir /etc/postfix/ssl
POSTFIX_SSL='/etc/postfix/ssl'

# Generate SSL certificate for Postfix
openssl genrsa -des3 -rand /etc/hosts -out $POSTFIX_SSL/smtpd.key 1024
chmod 600 $POSTFIX_SSL/smtpd.key
openssl req -new -key $POSTFIX_SSL/smtpd.key -out $POSTFIX_SSL/smtpd.csr
openssl x509 -req -days 3650 -in $POSTFIX_SSL/smtpd.csr -signkey $POSTFIX_SSL/smtpd.key -out $POSTFIX_SSL/smtpd.crt
openssl rsa -in $POSTFIX_SSL/smtpd.key -out $POSTFIX_SSL/smtpd.key.unencrypted
mv -f $POSTFIX_SSL/smtpd.key.unencrypted $POSTFIX_SSL/smtpd.key
openssl req -new -x509 -extensions v3_ca -keyout $POSTFIX_SSL/cakey.pem -out $POSTFIX_SSL/cacert.pem -days 3650
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
