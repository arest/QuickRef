#!/bin/bash

# Read the lines into an array
while read line
do
    array+=("$line")
done < some_file

# Remember that read trims leading and trailing whitespace, so if you wish to preserve it, clear the IFS variable:
while IFS= read -r line; do
    # do something with $line
done < file

# If you don't like the to put < file at the end, you can also pipe the contents of the file to the while loop:
cat file | while IFS= read -r line; do
    # do something with $line
done

# To access the items you would then use ${array[index]}, e.g.:
for ((i=0; i < ${#array[*]}; i++)); do
    echo "${array[i]}"
done

# Get size of thew array
echo ${#array[*]}

# Arrays
names=( Tom Jack )
for name in "${names[@]}"

# Safe word-splitting (no pathname expansion) and custom IFS limited to read command only
#IFS=, read -ra namesArray <<< "$names" 

for name in "${namesArray[@]}"; do 

# A while-read loop
#while read -rd name; do 
	#...; 
#done <<< "$names"

# Read a random line from a file and put it in a variable
read -r random_line < <(shuf file)

# Sometimes it's shorter to just write _ for the throwaway variable:
while read -r field1 field2 field3 _; do
	#  do something with $field1, $field2, and $field3
done < file

# Check regexp
if [[ "$cmdline" =~ $regex ]]; then
if [[ "$VAR_VALUE" == "y" ]] || [[ "$VAR_VALUE" == "yes" ]]
if [[ $line == *${date}* ]]; then
if [[ -n ${!1} ]]; then
[[ -n "${CURR_DEFAULT:-}" && -z "$DEFAULT" ]] && DEFAULT="${CURR_DEFAULT}"
# 
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
