# 6.10 Create CRL
openssl ca -gencrl -config etc/component-ca.conf -out crl/component-ca.crl

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
