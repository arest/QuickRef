
#Creating Certificates
#
#    Generate A Certificate Signing Request
 openssl req -new -newkey rsa:1024 -keyout hostkey.pem -nodes -out hostcsr.pem

#    Create A Self-Signed Certificate From A Certificate Signing Request
openssl req -x509 -days 365 -in hostcsr.pem -key hostkey.pem -out hostcert.pem

#    Generate A Self-Signed Certificate From Scratch
openssl req -x509 -days 365 -newkey rsa:1024 -keyout hostkey.pem -nodes -out hostcert.pem

#
#Viewing Certificates
#
#    View The Contents Of A Certificate Signing Request
openssl req -text -noout -in hostcsr.pem

#    View The Contents Of A Certificate
openssl x509 -text -noout -in hostcert.pem

#    View The Signer Of A Certificate
openssl x509 -in cert.pem -noout -issuer -issuer_hash

#    Verify A Certificate Matches A Private Key
openssl x509 -in cert.pem -noout -modulus
openssl rsa -in key.pem -noout -modulus

if [ "`openssl x509 -in cert.pem -noout -modulus`" = \
     "`openssl rsa -in key.pem -noout -modulus`" ]; \
     then echo "Match"; else echo "Different"; fi

#    Find The Hash Value Of A Certificate
openssl x509 -noout -hash -in cert.pem

ln -s cert.pem `openssl x509 -noout -hash -in cert.pem`.0

#
#Proxy Certificates
#
#    Allow Proxy Certificates For Client Verification
# csh
setenv OPENSSL_ALLOW_PROXY_CERTS 1
# bash
export OPENSSL_ALLOW_PROXY_CERTS=1
# Windows
set OPENSSL_ALLOW_PROXY_CERTS=1

#    Generate A Certificate Signing Request For A Proxy Certificate
openssl x509 -subject -noout -in cert.pem
subject= /C=US/O=NCSA/CN=Terry Fleury/emailAddress=tfleury@ncsa.uiuc.edu

openssl req -new -config csr.conf -out proxy.csr -keyout proxykey.pem

#    Create A Proxy Certificate From A Certificate Signing Request
openssl x509 -req -md5 -CAcreateserial -in proxy.csr -days 1 \
             -CA usercert.pem -CAkey userkey.pem -extfile csr.conf \
             -extensions v3_proxy -out proxycert.pem

cat proxycert.pem proxykey.pem usercert.pem > x509up_u_USERNAME

#
#Private Keys
#
#    Create A Private Key
 openssl genrsa -des3 -out key.pem 1024

#    Encrypt A Private Key
openssl rsa -des3 -in hostkeyNOPASSWORD.pem -out hostkeySECURE.pem

#    Decrypt A Private Key
openssl rsa -in hostkeySECURE.pem -out hostkeyNOPASSWORD.pem

#
#PKCS12 / PEM Formats
#
#    Convert PEM Format Certificate To PKCS12 Format Certificate
 openssl pkcs12 -export -in cert.pem -inkey key.pem -out cred.p12

#    Convert PKCS12 Format Certificate To PEM Format Certificate
openssl pkcs12 -in cred.p12 -out certkey.pem -nodes -clcerts

openssl pkcs12 -in cred.p12 -out cert.pem -nodes -clcerts -nokeys

#    View The Contents Of A PKCS12 Formatted File
openssl pkcs12 -info -nodes -in cred.p12
#
#Testing
#
#    Run A Test Server
openssl s_server -accept 9000 -cert hostcert.pem -key hostkey.pem

#    Run A Test Client 
openssl s_client -connect localhost:9000 -CApath /etc/grid-security/certificates


openssl s_client -verify_hostname arest-home.pp.ua -connect 192.168.20.70:443

openssl s_client -connect 192.168.20.70:443 -CAfile /etc/ssl/ca.crt

#Verify SMTP
openssl s_client -connect 192.168.20.70:25 -starttls smtp

#Debug 
openssl s_client -connect 192.168.20.70:25 -tlsextdebug



export OPENSSL_CONF=/etc/ssl/openssl.cnf

# Generate CA key and cert
openssl req -new -x509 -extensions v3_ca -keyout cakey.pem -out cacert.pem -days 3650

openssl x509 -in cacert.pem -out cacert.crt

# Create cert request
openssl req -new -key privkey.pem -out cert.csr

# Create self-signed cert
openssl req -new -x509 -key privkey.pem -out cert.pem -days 3650 

# Create cert req for server key
openssl req -newkey rsa:1024 -keyout tempkey.pem -keyform PEM -out tempreq.pem -outform PEM

# Clear password 
openssl rsa < tempkey.pem > arest-home.pp.ua.key.pem

# Sign cert by CA
openssl ca -in tempreq.pem -out arest-home.pp.ua.crt.pem

 openssl req -x509 -nodes -days 3650 -newkey rsa:1024 -keyout dr.arest@gmail.com.pem
openssl req -x509 -nodes -days 3650 -newkey rsa:1024 -keyout dr.arest@gmail.com.pem -out dr.arest@gmail.com.pem
openssl pkcs12 -in dr.arest@gmail.com.pem -out dr.arest@gmail.com.pfx -export -name "Main universal certificate for Whatever"

# Generate RSA key
openssl genrsa -des3 -out privkey.pem 2048

# Generate DSA key (2 step)
#  If you don't want your key to be protected by a password, remove the flag '-des3' from the command line above.
openssl dsaparam -out dsaparam.pem 2048
openssl gendsa -des3 -out privkey.pem dsaparam.pem


######################### PKCS11 #################################

## Installing engine_pkcs11 is quite simple:
>wget http://www.opensc-project.org/files/engine_pkcs11-x.y.z.tar.gz
tar xfvz engine_pkcs11-x.y.z.tar.gz
cd engine_pkcs11-x.y.z
./configure --prefix=/usr/
make
make install


## Using Engine_pkcs11 with the openssl command
You can run the OpenSSL command shell and load the engine and then run any command using the engine. Here is an example: 

<pre><code>$ openssl
OpenSSL> engine -t dynamic -pre SO_PATH:/usr/lib/engines/engine_pkcs11.so \
             -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD \
                      -pre MODULE_PATH:/usr/lib/opensc-pkcs11.so
OpenSSL> req -engine pkcs11 -new -key id_45 -keyform engine -out req.pem -text -x509 \
             -subj "/CN=Andreas Jellinghaus"
OpenSSL> x509 -engine pkcs11 -signkey slot_0-id_45 -keyform engine -in req.pem -out cert.pem
</code></pre>

In this example the engine_pkcs11 is loaded using the PKCS#11 module opensc-pkcs11.so. The second command creates a self signed Certificate for "Andreas Jellinghaus", the signing is done using the key with id 45 from your smart card in slot 0. The third command creates a self-signed certificate for the request, the private key used to sign the certificate is the same private key used to create the request. 
Using Engine_pkcs11 with the openssl config file

You can also create/edit an openssl config file, so you don't need to type in or paste the above commands all the time. Here is an example for OpenSSL 0.9.8:

<pre><code>openssl_conf            = openssl_def

[openssl_def]
engines = engine_section

[engine_section]
pkcs11 = pkcs11_section

[pkcs11_section]
engine_id = pkcs11
dynamic_path = /usr/lib/engines/engine_pkcs11.so
MODULE_PATH = /usr/lib/opensc-pkcs11.so
init = 0

[req]
distinguished_name = req_distinguished_name

[req_distinguished_name]
</code></pre>

With such a config file you can directly call openssl to use that engine:

<pre><code>openssl req -config openssl.conf -engine pkcs11 -new -key id_45 \
            -keyform engine -out req.pem -text -x509 \
                    -subj "/CN=Andreas Jellinghaus"
</code></pre>

## Engine PKCS#11 Options

### Options you can use with engine_pkcs11:
* **SO_PATH**: Specifies the path to the 'pkcs11-engine' shared library 
* **MODULE_PATH**: Specifies the path to the pkcs11 module shared library 
* **PIN**: Specifies the pin code 
* **VERBOSE**: Print additional details 
* **QUIET**: Remove additional details 
* **LOAD_CERT_CTRL**: Get the certificate from card 

**PIN** can be passed only in the [pkcs11_section] of the openssl.conf (see above).

**FIXME**: copied these options from the source code, untested

# OpenSSL autoloading
 OpenSSL 0.9.8+ can automaticaly load engines. If you want to enable that feature, add a symlink from engine_pkcs11.so to libfoo.so in the lib/engines/ directory where engine_pkcs11.so is installed. 

  We think that a config file is a much better approach, since you need to pass the PKCS#11 module to use to engine_pkcs11.so, and you can do that only via command line or via the config file.


# RuToken initialization on home-nas:

openssl engine dynamic -pre SO_PATH:/usr/lib/ssl/engines/libpkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:/usr/lib/librtpkcs11ecp.so

root@home-nas:/etc/ssl/CA# openssl engine dynamic -pre SO_PATH:/usr/lib/ssl/engines/libpkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:/usr/lib/librtpkcs11ecp.so 
(dynamic) Dynamic engine loading support
[Success]: SO_PATH:/usr/lib/ssl/engines/libpkcs11.so
[Success]: ID:pkcs11
[Success]: LIST_ADD:1
[Success]: LOAD
[Success]: MODULE_PATH:/usr/lib/librtpkcs11ecp.so
Loaded: (pkcs11) pkcs11 engine
root@home-nas:/etc/ssl/CA#

req -engine pkcs11 -new -key slot_1-id_04830838b7c9752bd0e90c96a88b989a80f45181 -keyform engine -out req.csengine dynamic -pre SO_PATH:/usr/lib/ssl/engines/libpkcs11.so -pre ID:pkcs11 -pre LIST_ADD:1 -pre LOAD -pre MODULE_PATH:/usr/lib/librtpkcs11ecp.so

openssl req -engine pkcs11 -new -key slot_0-id_d611734c09476a3215de3f4118dd014159a37114 -keyform engine -out req.csr -subj "/C=UA/ST=Lviv/L=Lviv/O=ArestInc/OU=CA/CN=Arest Inc. Root CA/emailAddress=support@arest-home.pp.ua"


# Convert private OpenSSL key to public OpenSSH key anf=d vice versa:

# OpenSSL >= 1.0.1 at least) use PKCS#8 format for keys.
openssl x509 -in certificate.pem -noout -pubkey >pubkey.pem

#You need to use following command to convert it to authorized_keys entry
ssh-keygen -i -m PKCS8 -f pubkey.pem

# To extract public key in the PKCS#8 format, understandable by import function of ssh-keygen use following command.
openssl req -in public.pem -noout -pubkey

# Convert .pub file into the .pem format
ssh-keygen  -e -m pem -f rsa.pub
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
