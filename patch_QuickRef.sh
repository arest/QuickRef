# Create patch
diff -u original_file new_file > patch_file.patch

# To use patch
patch original_file < patch_file.patch

# Undo all patch changes
patch -R original_file < patch_file.patch

# Create patch for all directory content
diff -ruN original_dir new_dir > patch_file.patch

# Apply directory patch
patch -p0 < patch_file.patch

# Undo directrory patch
patch -R -p0 original_file < patch_file.patch
# very strange. Looks like it should be
patch -R -p0 < patch_file.patch
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
