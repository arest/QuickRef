# 6.3 Create TLS client request
openssl req -new \
    -config etc/client.conf \
    -out certs/net-mon.csr \
    -keyout certs/net-mon.key
# DN: C=UA, O=Arest Inc., CN=Arest Network Monitoring

# 6.4 Create TLS client certificate
openssl ca \
    -config etc/component-ca.conf \
    -in certs/net-mon.csr \
    -out certs/net-mon.crt \
    -extensions client_ext

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
