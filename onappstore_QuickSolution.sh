########## ISSUE: on storagenode visible nodes only from local hypervisor 
########## SOLUTION: Set multicast_snooping => off
echo 0 > /sys/devices/virtual/net/onappstoresan/bridge/multicast_snooping

#Add the following to custom config:
#------%<--------------------------------------------
#turning off multicast snooping on onappstoresan
(bash -c "while [ 1 -eq 1 ] ; do if ip link show | grep -q onappstoresan ; then echo 0 > /sys/devices/virtual/net/onappstoresan/bridge/multicast_snooping; exit; fi ; sleep 20; done")&
#------%<--------------------------------------------
#======================================================================================================================

########## ISSUE:  "Too many frags" inside the storage node
########## SOLUTION: Set TSO => off

########## HOW TO CHECK:
# run on CP
for cbHV in `grep "fixed-address" /home/onapp/dhcpd.conf | cut -f 4 -d" " | sed 's/;//g'|sort -n`; do echo "===== $cbHV / "; ssh root@$cbHV 'for i in $(ls -1 /sys/class/net/onappstoresan/brif | grep -v onapp | grep -v eth); do ethtool -k $i 2>/dev/null |grep "tcp segmentation offload"; done' ; done

# run on HV
for i in `ls -1 /sys/class/net/onappstoresan/brif | grep -v onapp | grep -v eth`; do ethtool -K $i tso off; done

# add to custom config
#------%<--------------------------------------------
(sleep 600;for i in `ls -1 /sys/class/net/onappstoresan/brif | grep -v onapp | grep -v eth`; do ethtool -K $i tso off; done)&
#------%<--------------------------------------------
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
