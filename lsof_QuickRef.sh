
# List User Specific Opened Files
lsof -u root

# Find Processes running on Specific Port
lsof -i TCP:22

#  List Only IPv4 & IPv6 Open Files
lsof -i 4
lsof -i 6

# List Open Files of TCP Port ranges 1-1024 
lsof -i TCP:1-1024

# Exclude User with ‘^’ Character
lsof -i -u^root

# Find Out who’s Looking What Files and Commands?
lsof -i -u arest

# List all Network Connections
lsof -i

# Search by PID
lsof -p 1

# Kill all Activity of Particular User
kill -9 `lsof -t -u arest`
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
