ZONE=arest-home.loc
# Go to bind folder
cd /var/lib/bind

# Create a Zone Signing Key(ZSK) 
dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE $ZONE

# Create a Key Signing Key(KSK)
dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE $ZONE

# Add the public keys which contain the DNSKEY record to the zone file.
for key in `ls K${ZONE}*.key`; do
	echo "\$INCLUDE $key">> ${ZONE}.zone
done

# Sign the zone
dnssec-signzone -A -3 $(head -c 1000 /dev/random | sha1sum | cut -b 1-16) -N INCREMENT -o $ZONE -t ${ZONE}.zone

# Change in /etc/bind/named.conf.local from $ZONE.zone to $ZONE.zone.signed
sed -i 's/arest-home.loc.zone/&.signed/' /etc/bind/named.conf.local

# Reload bind
systemctl restart bind9

# Verify 
dig DNSKEY example.com. @localhost +multiline
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
