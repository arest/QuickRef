# Clone repo

git clone https://bitbucket.org/stefanholek/pki-example-3
cd pki-example-3/etc
patch -p0 -i ~/SCRIPT/ssl/pki-example-3-Arest.patch
cd ..


# 1. Create Rooto CA

# 1.1 Create directories
mkdir -p ca/root-ca/private ca/root-ca/db crl certs
chmod 700 ca/root-ca/private

# 1.2 Create database
cp /dev/null ca/root-ca/db/root-ca.db
cp /dev/null ca/root-ca/db/root-ca.db.attr
echo 01 > ca/root-ca/db/root-ca.crt.srl
echo 01 > ca/root-ca/db/root-ca.crl.srl

# 1.3 Create CA request
openssl req -new \
    -config etc/root-ca.conf \
    -out ca/root-ca.csr \
    -keyout ca/root-ca/private/root-ca.key

# 1.4 Create CA certificate
openssl ca -selfsign \
    -config etc/root-ca.conf \
    -in ca/root-ca.csr \
    -out ca/root-ca.crt \
    -extensions root_ca_ext \
    -enddate 20301231235959Z

# 1.5 Create initial CRL
openssl ca -gencrl \
    -config etc/root-ca.conf \
    -out crl/root-ca.crl

## 2. Create Network CA

# 2.1 Create directories
#mkdir -p ca/network-ca/private ca/network-ca/db crl certs
#chmod 700 ca/network-ca/private

# 2.2 Create database
#cp /dev/null ca/network-ca/db/network-ca.db
#cp /dev/null ca/network-ca/db/network-ca.db.attr
#echo 01 > ca/network-ca/db/network-ca.crt.srl
#echo 01 > ca/network-ca/db/network-ca.crl.srl

# 2.3 Create CA request
#openssl req -new \
#    -config etc/network-ca.conf \
#    -out ca/network-ca.csr \
#    -keyout ca/network-ca/private/network-ca.key

# 2.4 Create CA certificate
#openssl ca \
#    -config etc/root-ca.conf \
#    -in ca/network-ca.csr \
#    -out ca/network-ca.crt \
#    -extensions intermediate_ca_ext \
#    -enddate 20301231235959Z
# Intermediate CAs should have the same life span as their root CAs.

# 2.5 Create initial CRL
#openssl ca -gencrl \
#    -config etc/network-ca.conf \
#    -out crl/network-ca.crl

# 2.6 Create PEM bundle
#cat ca/network-ca.crt ca/root-ca.crt > \
#    ca/network-ca-chain.pem

# 3. Create Identity CA
# 3.1 Create directories

mkdir -p ca/identity-ca/private ca/identity-ca/db crl certs
chmod 700 ca/identity-ca/private

#3.2 Create database
cp /dev/null ca/identity-ca/db/identity-ca.db
cp /dev/null ca/identity-ca/db/identity-ca.db.attr
echo 01 > ca/identity-ca/db/identity-ca.crt.srl
echo 01 > ca/identity-ca/db/identity-ca.crl.srl
 
# 3.3 Create CA request
openssl req -new \
    -config etc/identity-ca.conf \
    -out ca/identity-ca.csr \
    -keyout ca/identity-ca/private/identity-ca.key

# 3.4 Create CA certificate
openssl ca \
    -config etc/root-ca.conf \
    -in ca/identity-ca.csr \
    -out ca/identity-ca.crt \
    -extensions signing_ca_ext

# 3.5 Create initial CRL
openssl ca -gencrl \
    -config etc/identity-ca.conf \
    -out crl/identity-ca.crl

# 3.6 Create PEM bundle
cat ca/identity-ca.crt ca/root-ca.crt > \
   ca/identity-ca-chain.pem

# 4. Create Component CA

# 4.1 Create directories
mkdir -p ca/component-ca/private ca/component-ca/db crl certs
chmod 700 ca/component-ca/private

# 4.2 Create database
cp /dev/null ca/component-ca/db/component-ca.db
cp /dev/null ca/component-ca/db/component-ca.db.attr
echo 01 > ca/component-ca/db/component-ca.crt.srl
echo 01 > ca/component-ca/db/component-ca.crl.srl

# 4.3 Create CA request
openssl req -new \
    -config etc/component-ca.conf \
    -out ca/component-ca.csr \
    -keyout ca/component-ca/private/component-ca.key

# 4.4 Create CA certificate
openssl ca \
    -config etc/root-ca.conf \
    -in ca/component-ca.csr \
    -out ca/component-ca.crt \
    -extensions signing_ca_ext

# 4.5 Create initial CRL
openssl ca -gencrl \
    -config etc/component-ca.conf \
    -out crl/component-ca.crl

# 4.6 Create PEM bundle
cat ca/component-ca.crt ca/root-ca.crt > \
    ca/component-ca-chain.pem

# 5. Operate Identity CA

# 5.1 Create identity request
openssl req -new \
    -config etc/identity.conf \
    -out certs/arest@arest-home.pp.ua.csr \
    -keyout certs/arest@arest-home.pp.ua.key


# DN: C=UA, O=Arest Inc., CN=Orest Pazdriy, emailAddress=arest@arest-home.pp.ua

# 5.2 Create client certificate  
openssl ca \
    -config etc/identity-ca.conf \
    -in certs/Orest_Pazdriy.csr \
    -out certs/Orest_Pazdriy.crt \
    -extensions client_ext

# 5.3 Create PKCS#12 bundle
openssl pkcs12 -export \
    -name "Orest Pazdriy (Arest Inc.)" \
    -caname "Arest Identity CA" \
    -caname "Arest Root CA" \
    -inkey certs/Orest_Pazdriy.key \
    -in certs/Orest_Pazdriy.crt \
    -certfile ca/identity-ca-chain.pem \
    -out certs/Orest_Pazdriy.p12

# 5.7 Revoke certificate
#openssl ca \
#    -config etc/identity-ca.conf \
#    -revoke ca/identity-ca/02.pem \
#    -crl_reason superseded

# 5.8 Create CRL
openssl ca -gencrl \
    -config etc/identity-ca.conf \
    -out crl/identity-ca.crl

# 6. Operate Component CA

# 6.1 Create TLS server request
SAN=DNS:arest-home.pp.ua,DNS:www.arest-home.pp.ua,DNS:ftp.arest-home.pp.ua,DNS:mail.arest-home.pp.ua,DNS:radius.arest-home.pp.ua,DNS:webmin.arest-home.pp.ua,DNS:home-nas.arest-home.pp.ua,DNS:ldap.arest-home.pp.ua,DNS:*.arest-home.pp.ua,DNS:arest-home.local,DNS:*.arest-home.local,IP:193.93.219.55,IP:192.168.51.70 \
openssl req -new \
    -config etc/server.conf \
    -out certs/home-nas.arest-home.pp.ua.csr \
    -keyout certs/home-nas.arest-home.pp.ua.key
# DN: C=SE, O=Arest Inc., CN=www.arest-home.pp.ua

# 6.2 Create TLS server certificate
openssl ca \
    -config etc/component-ca.conf \
    -in certs/home-nas.arest-home.pp.ua.csr \
    -out certs/home-nas.arest-home.pp.ua.crt \
    -extensions server_ext

# 6.3 Create TLS client request
openssl req -new \
    -config etc/client.conf \
    -out certs/net-mon.csr \
    -keyout certs/net-mon.key
# DN: C=UA, O=Arest Inc., CN=Arest Network Monitoring

# 6.4 Create TLS client certificate
openssl ca \
    -config etc/component-ca.conf \
    -in certs/net-mon.csr \
    -out certs/net-mon.crt \
    -extensions client_ext

# 6.5 Create time-stamping request
openssl req -new \
    -config etc/timestamp.conf \
    -out certs/tsa.csr \
    -keyout certs/tsa.key
# DN: C=UA, O=Arest Inc., OU=Arest TSA, CN=Arest TSA

# 6.6 Create time-stamping certificate
openssl ca \
    -config etc/component-ca.conf \
    -in certs/tsa.csr \
    -out certs/tsa.crt \
    -extensions timestamp_ext \
    -days 1826

# 6.7 Create OCSP-signing request
openssl req -new \
    -config etc/ocspsign.conf \
    -out certs/ocsp.csr \
    -keyout certs/ocsp.key
# DN: C=UA, O=Arest Inc., CN=Arest OCSP Responder

# 6.8 Create OCSP-signing certificate
openssl ca \
    -config etc/component-ca.conf \
    -in certs/ocsp.csr \
    -out certs/ocsp.crt \
    -extensions ocspsign_ext \
    -days 14

# 6.9 Revoke certificate
openssl ca \
    -config etc/component-ca.conf \
    -revoke ca/component-ca/02.pem \
    -crl_reason superseded

# 6.10 Create CRL
openssl ca -gencrl \
    -config etc/component-ca.conf \
    -out crl/component-ca.crl

# 7. Publish Certificates

# 7.1 Create DER certificate
openssl x509 \
    -in ca/root-ca.crt \
    -out ca/root-ca.cer \
    -outform der
# All published certificates must be in DER format. MIME type: application/pkix-cert. [RFC 2585#section-4.1]

# 7.2 Create DER CRL
openssl crl \
    -in crl/network-ca.crl \
    -out crl/network-ca.crl \
    -outform der
# All published CRLs must be in DER format. MIME type: application/pkix-crl. [RFC 2585#section-4.2]

# 7.3 Create PKCS#7 bundle
openssl crl2pkcs7 -nocrl \
    -certfile ca/identity-ca-chain.pem \
    -out ca/identity-ca-chain.p7c \
    -outform der
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
