p11tool --list-tokens
#Token 0:
#	URL: pkcs11:model=p11-kit-trust;manufacturer=PKCS%2311%20Kit;serial=1;token=System%20Trust
#	Label: System Trust
#	Type: Trust module
#	Manufacturer: PKCS#11 Kit
#	Model: p11-kit-trust
#	Serial: 1
#	Module: p11-kit-trust.so
#
#
#Token 1:
#	URL: pkcs11:model=Rutoken%20ECP;manufacturer=Aktiv%20Co.;serial=32775dae;token=Rutoken%20ECP%20%3cno%20label%3e
#	Label: Rutoken ECP <no label>
#	Type: Hardware token
#	Manufacturer: Aktiv Co.
#	Model: Rutoken ECP
#	Serial: 32775dae
#	Module: /usr/lib/librtpkcs11ecp.so
#

p11tool --list-all
#warning: no token URL was provided for this operation; the available tokens are:
#
#pkcs11:model=p11-kit-trust;manufacturer=PKCS%2311%20Kit;serial=1;token=System%20Trust
#pkcs11:model=Rutoken%20ECP;manufacturer=Aktiv%20Co.;serial=32775dae;token=Rutoken%20ECP%20%3cno%20label%3e

# Initialize the device
pkcs11-tool --module /usr/local/lib/opensc-pkcs11.so --init-token --init-pin --so-pin=3537363231383830 --new-pin=648219 --label="test" --pin=648219

# change the SO-PIN
pkcs11-tool --module /usr/local/lib/opensc-pkcs11.so --login --login-type so --so-pin 3537363231383830 --change-pin --new-pin 0123456789012345

# change the PIN
pkcs11-tool --login --pin 648219 --change-pin --new-pin 123456

# generate a key pair
pkcs11-tool --module /usr/local/lib/opensc-pkcs11.so -l --pin 648219 --keypairgen --key-type rsa:1024 --id 10

# or for ECC keys:
pkcs11-tool —module /usr/local/lib/opensc-pkcs11.so —login —pin 648219 —keypairgen —key-type EC:prime256v1 —label mykey

# save the generated public key in Subject Public Key Information format as per RF3280
pkcs11-tool --module /usr/local/lib/opensc-pkcs11.so -l --pin 648219 --id 10 --read-object --type pubkey --output-file pubkey.spki

# store certificates and data
pkcs11-tool --module /usr/local/lib/opensc-pkcs11.so -l --pin 648219 --write-object testcert.der --type cert --id 10
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
