# Add schema
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/gosa/trust.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/pureftpd.schema
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/gosa/nagios.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/gosa/pureftpd.ldif

# Add addressbook data
ldapadd -x -D 'cn=admin,dc=arest-home,dc=pp,dc=ua' -W -f ~/ldap_addressbook.ldif

# Search
ldapsearch -x  '(objectclass=*)'
ldapsearch -x  -H ldaps://arest-home.pp.ua -b dc=arest-home,dc=pp,dc=ua
ldapsearch -x -LLL -b dc=arest-home,dc=pp,dc=ua 'uid=arest' cn gidNumber
# -x: "простое" связывание; не будет использоваться метод SASL по умолчанию
# -LLL: отключить вывод посторонней информации
# uid=john: "фильтр" для нахождения пользователя john
# cn gidNumber: запрос на вывод определенных атрибутов (по умолчанию выводятся все атрибуты)

sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config '(olcDatabase={1}hdb)' olcAccess

# Search for config, schema
sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=schema,cn=config dn
sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=schema,cn=config 'cn=*samba*'

# Add schema
sudo cp /usr/share/doc/samba-doc/examples/LDAP/samba.schema.gz /etc/ldap/schema
sudo gzip -d /etc/ldap/schema/samba.schema.gz
cd /etc/ldap
#create schema_convert.conf:
#
echo "include /etc/ldap/schema/core.schema" >schema_convert.conf
echo "include /etc/ldap/schema/collective.schema" >schema_convert.conf
echo "include /etc/ldap/schema/corba.schema" >schema_convert.conf
echo "include /etc/ldap/schema/cosine.schema" >schema_convert.conf
echo "include /etc/ldap/schema/duaconf.schema" >schema_convert.conf
echo "include /etc/ldap/schema/dyngroup.schema" >schema_convert.conf
echo "include /etc/ldap/schema/inetorgperson.schema" >schema_convert.conf
echo "include /etc/ldap/schema/java.schema" >schema_convert.conf
echo "include /etc/ldap/schema/misc.schema" >schema_convert.conf
echo "include /etc/ldap/schema/nis.schema" >schema_convert.conf
echo "include /etc/ldap/schema/openldap.schema" >schema_convert.conf
echo "include /etc/ldap/schema/ppolicy.schema" >schema_convert.conf
echo "include /etc/ldap/schema/ldapns.schema" >schema_convert.conf
echo "include /etc/ldap/schema/pmi.schema" >schema_convert.conf
echo "include /etc/ldap/schema/samba.schema" >schema_convert.conf
#
slapcat -f schema_convert.conf -F ldif_output -n0 -H ldap:///cn={14}samba,cn=schema,cn=config -l cn=samba.ldif
sudo ldapadd -Q -Y EXTERNAL -H ldapi:/// -f cn\=samba.ldif


# Search for indexes
# older db hdb
sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config olcDatabase={1}hdb olcDbIndex
# or newer db mdb
sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config olcDatabase={1}mdb olcDbIndex


# Delete schema
ldapdelete -Y EXTERNAL cn=samba,cn=schema,cn=config
ldapdelete -Y EXTERNAL -H ldapi:///  cn=samba,cn=schema,cn=config


# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
