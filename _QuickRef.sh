# SORT
ls -al | sort -nk +5			# sort output by 5th row asc
ls -al | sort -nrk +5			# sort output by 5th row desc
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
