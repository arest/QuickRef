#In this example, it displays the A record of redhat.com in the “ANSWER SECTION” of the dig command output.
dig redhat.com

# Display Only the ANSWER SECTION of the Dig command Output
#   +nocomments – Turn off the comment lines
#   +noauthority – Turn off the authority section
#   +noadditional – Turn off the additional section
#   +nostats – Turn off the stats section
#   +noanswer – Turn off the answer section (Of course, you wouldn’t want to turn off the answer section)

# The following dig command displays only the ANSWER SECTION.
dig redhat.com +nocomments +noquestion +noauthority +noadditional +nostats


# The above command can also be written in a short form as shown below, which displays only the ANSWER SECTION.
dig redhat.com +noall +answer


# Query MX Records Using dig -t MX
dig redhat.com  MX +noall +answer

# You can also use option -t to pass the query type (for example: MX) as shown below.
dig -t MX redhat.com +noall +answer

# Query NS Records Using dig -t NS
dig redhat.com NS +noall +answer
dig -t NS redhat.com +noall +answer

# View ALL DNS Records Types Using dig -t ANY
dig redhat.com ANY +noall +answer
dig -t ANY redhat.com  +noall +answer

# View Short Output Using dig +short
dig redhat.com +short
# You can also specify a record type that you want to view with the +short option.
dig redhat.com ns +short

# DNS Reverse Look-up Using dig -x
dig -x 209.132.183.81 +short

# To view the full details of the DNS reverse look-up, remove the +short option.
dig -x 209.132.183.81

# Use a Specific DNS server Using dig @dnsserver
dig @ns1.redhat.com redhat.com

# Bulk DNS Query Using dig -f (and command line)
dig -f names.txt +noall +answer

dig -f names.txt MX +noall +answer

dig redhat.com mx +noall +answer centos.org ns +noall +answer


# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
