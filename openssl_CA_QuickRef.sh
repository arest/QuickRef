# Build CA

# 1. Create Root CA config 

cat <<CONF-EOF
[default]
name					= ArestRootCA
domain_suffix			= arest-home.pp.ua
aia_url					= http://$name.$domain_suffix/$name.crt
crl_url					= http://$name.$domain_suffix/$name.crl
ocsp_url				= http://ocsp.$name.$domain_suffix:9080
default_ca				= ca_default
name_opt				= utf8,esc_ctrl,multiline,lname,align

[ca_dn]
countryName				= "UA"
organizationName		= "Arest Inc."
commonName				= "Root CA"

[ca_default]
home					= .
database				= $home/db/index
serial					= $home/db/serial
crlnumber				= $home/db/crlnumber
certificate				= $home/$name.crt
private_key				= $home/private/$name.key


RANDFILE				= $home/private/random
new_certs_dir			= $home/certs
unique_subject			= no
copy_extensions			= none
default_days			= 3650
default_crl_days		= 365
default_md				= sha256
policy					= policy_c_o_match


[policy_c_o_match]
countryName				= match
stateOrProvinceName		= optional
organizationName		= match
organizationalUnitName 	= optional
commonName				= supplied
emailAddress			= optional


[req]
default_bits			= 2048
encrypt_key				= yes
default_md				= sha256
utf8					= yes
string_mask				= utf8only
prompt					= no
distinguished_name		= ca_dn
req_extensions			= ca_ext

[ca_ext]
basicConstraints		= critical,CA:true
keyUsage				= critical,keyCertSign,cRLSign
subjectKeyIdentifier	= hash


[sub_ca_ext]
authorityInfoAccess		= @issuer_info
authorityKeyIdentifier	= keyid:always
basicConstraints		= critical,CA:true,pathlen:0
crlDistributionPoints	= @crl_info
extendedKeyUsage		= clientAuth,serverAuth
keyUsage				= critical,keyCertSign,cRLSign
#nameConstraints			= @name_constraints
subjectKeyIdentifier 	= hash


[crl_info]
URI.0					= $crl_url

[issuer_info]
caIssuers;URI.0			= $aia_url
OCSP;URI.0 				= $ocsp_url

[name_constraints]
#permitted;DNS.0			= arest-home.pp.ua
#permitted;DNS.1			= *.arest-home.pp.ua
#excluded;IP.0			= 0.0.0.0/0.0.0.0
#excluded;IP.1			= 0:0:0:0:0:0:0:0/0:0:0:0:0:0:0:0


[ocsp_ext]

authorityKeyIdentifier	= keyid:always
basicConstraints		= critical,CA:false
extendedKeyUsage		= OCSPSigning
keyUsage				= critical,digitalSignature
subjectKeyIdentifier	= hash

[ additional_oids ]
ArestInc        = Arest Inc., 1.3.6.1.4.1.47851.1.1

CONF-EOF

# 2. Create dirs
DIR=/etc/ssl
CADIR=$DIR/root-ca
CONF=$CADIR/root-ca.conf
CA=ArestRootCA
CERT=$CADIR/$CA.crt
KEY=$CADIR/private/$CA.key
CSR=$FCADIR/private/$CA.csr
export DIR CADIR CONF CA CERT KEY CSR

rm -rf $CADIR
mkdir $CADIR
cp root-ca.conf $CADIR
cd $CADIR
mkdir certs db private csr
chmod 700 private
touch $CADIR/db/index
openssl rand -hex 16 > $CADIR/db/serial
echo 1001 > $CADIR/db/crlnumber
cd -

# 3. Root CA Generation
openssl req -new \
	-config $CONF \
	-out $CSR \
	-keyout $KEY

openssl ca -selfsign \
	-config $CONF \
	-in $CSR \
	-out $CERT \
	-extensions ca_ext

# To generate a CRL from the new CA
openssl ca -gencrl \
	-config $CONF \
	-out $CADIR/$CA.crl


#openssl ca \
#	-config openssl.cnf \
#	-in sub-ca.csr \
#	-out sub-ca.crt \
#	-extensions sub_ca_ext


# To revoke a certificate, use the -revoke switch of the ca command; you’ll need to have a copy
# of the certificate you wish to revoke. Because all certificates are stored in the certs/ directory,
# you only need to know the serial number. If you have a distinguished name, you can look for
# the serial number in the database.
# Choose the correct reason for the value in the -crl_reason switch. The value can be one of
# the following: 
#
#    unspecified , keyCompromise , CACompromise , affiliationChanged , superseded ,
#    cessationOfOperation , certificateHold, and removeFromCRL .

#openssl ca \
#	-config $CONF \
#	-revoke $CADIR/certs/1002.pem \
#	-crl_reason keyCompromise


# Create a Certificate for OCSP Signing
openssl req -new \
	-newkey rsa:2048 \
	-subj "/C=UA/O=Arest Inc./CN=OCSP Root Responder" \
	-keyout $CADIR/private/$CA-OCSP.key \
	-out $CADIR/csr/$CA-OCSP.csr

openssl ca \
	-config $CONF \
	-in $CADIR/csr/$CA-OCSP.csr \
	-out $CADIR/$CA-OCSP.crt \
	-extensions ocsp_ext \
	-days 365

# Now you have everything ready to start the OCSP responder. For testing, you can do it from
# the same machine on which the root CA resides. However, for production you must move the
# OCSP responder key and certificate elsewhere:

openssl ocsp \
	-port 9080 \
	-index $CADIR/db/index \
	-rsigner $CADIR/$CA-OCSP.crt \
	-rkey $CADIR/private/$CA-OCSP.key \
	-CA $CADIR/$CA.crt \
	-text

# You can test the operation of the OCSP responder using the following command line:
openssl ocsp \
	-issuer $CADIR/$CA.crt \
	-CAfile $CADIR/$CA.crt \
	-cert $CADIR/$CA-OCSP.crt \
	-url http://127.0.0.1:9080


# Subordinate CA Configuration
# To generate a configuration file for the subordinate CA, start with the file we used for the
# root CA and make the changes listed here. We’ll change the name to sub-ca and use a differ-
# ent distinguished name. We’ll put the OCSP responder on a different port, but only because
# the ocsp command doesn’t understand virtual hosts. If you used a proper web server for the
# Creating a Subordinate CA
# 45OCSP responder, you could avoid using special ports altogether. The default lifetime of new
# certificates will be 365 days, and we’ll generate a fresh CRL once every 30 days.
# The change of copy_extensions to copy means that extensions from the CSR will be copied
# into the certificate, but only if they are not already set in our configuration. With this change,
# whoever is preparing the CSR can put the required alternative names in it, and the information
# from there will be picked up and placed in the certificate. This feature is somewhat dangerous
# (you’re allowing someone else to have limited direct control over what goes into a certificate),
# but I think it’s fine for smaller environments:
cp root-ca.conf sub-ca.conf
sed -i 's/root-ca/sub-ca/' sub-ca.conf
awk '/name/{gsub(/root-ca/, "sub-ca"
[default]
name 					= sub-ca
ocsp_url 				= http://ocsp.$name.$domain_suffix:9081

[ca_dn]
countryName				= "UA"
organizationName 		= "Example"
commonName 				= "Sub CA"

[ca_default]
default_days			= 365
default_crl_days		= 30
copy_extensions 		= copy



[server_ext]
authorityInfoAccess		= @issuer_info
authorityKeyIdentifier	= keyid:always
basicConstraints		= critical,CA:false
crlDistributionPoints	= @crl_info
extendedKeyUsage		= clientAuth,serverAuth
keyUsage				= critical,digitalSignature,keyEncipherment
subjectKeyIdentifier 	= hash


[client_ext]
authorityInfoAccess 	= @issuer_info
authorityKeyIdentifier	= keyid:always
basicConstraints		= critical,CA:false
crlDistributionPoint	= @crl_info
extendedKeyUsage		= clientAuth
keyUsage 				= critical,digitalSignature
subjectKeyIdentifier 	= hash


# Subordinate CA Generation
SUBCA=$DIR/sub-ca
SUBCONF=$SUBCA/sub-ca.conf
mkdir $SUBCA
cd $SUBCA
mkdir certs db private csr
chmod 700 private
touch db/index
openssl rand -hex 16 > db/serial
echo 1001 > db/crlnumber

openssl req -new \
	-config $SUBCONF \
	-out $SUBCA/csr/ArestSubCA.csr \
	-keyout $SUBCA/private/ArestSubCA.key

cd ../$CADIR

openssl ca \
	-config $CONF \
	-in $SUBCA/csr/ArestSubCA.csr \
	-out $SUBCA/ArestSubCA.crt \
	-extensions sub_ca_ext

cd $SUBCA

# To issue a server certificate


openssl req -new \
	-newkey rsa:2048 \
	-nodes \
#	-subj "/C=UA/O=Arest Inc./CN=home-nas.local" \
	-keyout $SUBCA/private/arest-home.pp.ua.key \
	-reqexts server_ext \
	-out $SUBCA/arest-home.pp.ua.csr

openssl ca \
	-config $SUBCONF
	-in $SUBCA/csr/arest-home.pp.ua.csr \
	-out $SUBCA/arest-home.pp.ua.crt \
	-extensions server_ext

# To issue a client certificate

openssl req -new \
	-newkey rsa:2048 \
	-nodes \
#	-subj "/C=UA/O=Arest Inc./CN=dr.arest@gmail.com" \
	-keyout $SUBCA/private/dr.arest@gmail.com.key \
	-out $SUBCA/csr/Orest_Pazdriy.csr

openssl ca \
	-config $SUBCONF \
	-in $SUBCA/csr/Orest_Pazdriy.csr \
	-out $SUBCA/Orest_Pazdriy.crt \
	-extensions megaclient_ext


openssl x509 -in $CADIR/$CA.crt -out $CADIR/$CA.crt
openssl x509 -outform DER < $CADIR/$CA.crt > $CADIR/$CA.cer

openssl x509 -trustout < $CADIR/$CA.crt > $CADIR/$CA.trusted.crt
openssl x509 -outform DER < $CADIR/$CA.trusted.crt > $CADIR/$CA.trusted.cer

openssl rsa -pubout < private/$CA.key > $CA.pub
openssl rsa -pubin -in $CA.pub -pubout -outform DER -out $CA.pub.cer
openssl rsa -RSAPublicKey_out < private/$CA.key > $CA.rsa.pub

pkcs11-tool -y cert -w $CA.trusted.cer --label $CA --id 1 --attr-from $CA.trusted.cer --pin 3128443
pkcs11-tool -y privkey -w private/$CA.key.cer --label $CA --id 1 --attr-from $CA.trusted.cer --pin 3128443 -l
pkcs11-tool -y pubkey -w $CA.pub.cer --label $CA --id 1 --attr-from $CA.trusted.cer --pin 3128443


openssl x509 -in ArestSubCA.crt -out ArestSubCA.crt
openssl x509 -outform DER < ArestSubCA.crt > ArestSubCA.cer

openssl x509 -trustout < ArestSubCA.crt > ArestSubCA.trusted.crt
openssl x509 -outform DER < ArestSubCA.trusted.crt > ArestSubCA.trusted.cer

openssl rsa -pubout < private/ArestSubCA.key > ArestSubCA.pub
openssl rsa -pubin -in ArestSubCA.pub -pubout -outform DER -out ArestSubCA.pub.cer
openssl rsa -RSAPublicKey_out < private/ArestSubCA.key > ArestSubCA.rsa.pub

openssl rsa -in private/ArestSubCA.key -out private/ArestSubCA.key.cer -outform DER
openssl x509 -in ArestSubCA.crt -out ArestSubCA.cer -outform DER
#ssh-keygen -i -f ArestSubCA.pub -m PKCS8 > ArestSubCA.ssh.pub

pkcs11-tool -y cert -w ArestSubCA.trusted.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
pkcs11-tool -y privkey -w private/ArestSubCA.key.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
pkcs11-tool -y pubkey -w ArestSubCA.pub.cer --label ArestSubCA --id 2 --attr-from ArestSubCA.trusted.cer --pin 3128443 -l
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
