echo  Edit /etc/bind/named.conf.options:
echo	dnssec-enable	yes;
echo	dnssec-validation	yes;
echo	dnssec-lookaside	auto;

#ZONE=arest-home.loc
ZONE=$1
ZONEFILE=/var/lib/bind/${ZONE}.zone

echo -n "Pushing current dir "
pushd .
cd /var/lib/bind

# Create Zone Signing Key (ZSK)
dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE $ZONE

# Create Key Signing Key (KSK)
dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE $ZONE

# Add keys to zone file 
for key in `ls /var/lib/bind/K${ZONE}*.key`; do
	echo "\$INCLUDE $key" >> ${ZONEFILE}
done

# Sign the zone
dnssec-signzone -A -3 $(head -c 1000 /dev/random |sha1sum |cut -b 1-16) -N INCREMENT -o $ZONE -t ${ZONEFILE}

# edit zone file
sed -i 's|file \"\/var\/lib\/bind\/${ZONE}.zone|&\.signed/|g' ${ZONEFILE}

chown bind. K${ZONE}*.key

# Reload BIND
systemctl reload bind9


echo "Restoring working directory "
popd 
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
