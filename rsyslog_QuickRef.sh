# Write each host to own logfile
$template PerHostLog,"/var/log/%HOSTNAME%.log"
if $fromhost-ip startswith '10.0.9.' then -?PerHostLog
& ~

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
