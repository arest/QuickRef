# Initialize tripwire
sudo tripwire --init

sudo sh -c "tripwire --check | grep Filename > missing-directory.txt"

# Edit /etc/tripwire/twpol.txt
vi /etc/tripwire/twpol.txt

# or use a patch tripwire-2.4.3.1-twpol.txt.patch
patch /etc/tripwire/twpol.txt < tripwire-2.4.3.1-twpol.txt.patch



# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
