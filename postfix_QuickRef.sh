# Check current active  mail queue
mailq

# check mail queue
postqueue -p

# flash/resend the queue mails
postqueue -f

# check the basic config
postconf -n

# check whole config
postconf

# make all queue as renew queue
postsuper -r ALL

# Get email accounts with high Postfix queues
mailq|grep ^[A-F0-9]|cut -c 42-80|sort |uniq -c|sort -n|tail

# Delete one message with the named queue ID from the named mail queue(s)
# (default: hold, incoming, active and deferred).
postsuper -d queue_id

#  Put mail “on hold” so that no attempt is made to deliver it. i
# Move one message with the named queue ID from the named mail queue(s) – 
# (default: incoming, active and deferred) to the hold queue.  
postsuper -h queue_id

# Release mail that was put “on hold”. Move one message with the named 
# queue ID from the named mail queue(s) – (default: hold) to the deferred queue.
postsuper -H queue_id

# Requeue the message with the named queue ID from the named mail queue(s)-
# (default: hold, incoming, active and deferred).
postsuper -r queue_id

# Postfix hold all deferred messages
postsuper -h ALL deferred

# Postfix remove all messages on hold
postsuper -d ALL hold

# Postfix release all messages on HOLD.
postsuper -H ALL

# Postfix dump all mails back to queue (Requeue)
postsuper -r ALL

# Postfix purge old temporary files
postsuper -p


# Flushing queue with postqueue
postqueue -f

# Print  postfix queue in sendmail-style
postqueue -p

# Postfix print queue in JSON format
postqueue -j

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
