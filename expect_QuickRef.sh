#!/usr/bin/expect -f

# wait forever (positive number would be # of seconds)
set timeout 5

send_user "Machine Name/Address: "
expect_user -re "(.*)\n" {set MACHADDR $expect_out(1,string)}
#set MACHADDR "192.168.1.1"

send_user "Machine Name / Address is $MACHADDR\n"

send_user "Login: "
expect_user -re "(.*)\n" {set USERNAME $expect_out(1,string)}

send_user "Password: "
stty -echo
expect_user -re "(.*)\n" {set PASSWORD $expect_out(1,string)}
send_user "\n"
stty echo

#telnets to a term svr
spawn telnet $MACHADDR

#sleep 2
#send "\r"

expect "login"
send "$USERNAME\r"

expect "password"
send "$PASSWORD\r"

expect "LOGGED IN"

Math
-----

set abc [expr 1 + 1]
set abc [expr $abc + 2]

If
---

if {$i == 4} {
	send_user "i is $i\n"
} elseif {[regexp \[aeiou\] $c}
	send_user "c has a vowel"
} else {
	send_user "do something else"
}

While
-----

set LETTER " "
while { $LETTER != "q" } {
	expect_user -re "(.*}\n" {set LETTER $expect_out(1,string)}
	send_user "you typed $LETTER\n"
}

For
----

for {set i 1} {$i <= 5} {set i [expr $i + 1]} {
    send_user "$i\n"
}

Read lines from file
From http://www.wellho.net/forum/The-Tcl-programming-language/expect-reading-in-a-list.html.
------------------------

set fhandle [open dafile r]

while {[gets $fhandle inline] >= 0} {
    switch -glob -- $inline {
    Name:*  {
            set name [lindex $inline 1]
            }
    Address:* {
            set address($name) [lindex $inline 1]
                }
    }
}
close $fhandle
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
