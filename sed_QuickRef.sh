sed -i '/kernel \/xen.gz-3.4.4 dom0_mem=1374226$/s/dom0_mem[[:space:]]*=[[:space:]]*[[:digit:]]\+/dom0_mem=1397504/' /boot/grub/grub.conf

sed 's/string1/string2/g'                    # Replace string1 with string2
sed -i 's/wroong/wrong/g' *.txt              # Replace a recurring word with g
sed 's/\(.*\)1/\12/g'                        # Modify anystring1 to anystring2
sed '/<p>/,/<\/p>/d' t.xhtml                 # Delete lines that start with <p> and end with </p>
sed '/ *#/d; /^ *$/d'                        # Remove comments and blank lines
sed 's/[ \t]*$//'                            # Remove trailing spaces (use tab as \t)
sed 's/^[ \t]*//;s/[ \t]*$//'                # Remove leading and trailing spaces
sed 's/[^*]/[&]/'                            # Enclose first char with [] top->[t]op
sed = file | sed 'N;s/\n/\t/' > file.num     # Number lines on a file
sed -i 's/\x1b\[[0-9;]*m//g' ansi_colors.txt # Remove ansi colors from text
sed -i 1,42d dump.sql						 # Remove first 42 lines 
sed 1,42d dump.sql							 # analogue: tail -n +43 dump.sql
####
#### 	Regular expression examples:
####
#================================================
#	Item		| 	Use regexp	|
#================================================	
#	'		|	'\''
#			|			|	
#			|			|	
#================================================
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
