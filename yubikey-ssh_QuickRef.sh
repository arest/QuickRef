# Generate an "ssh user CA" key and trust it for this account on this host
ssh-keygen -N '' -C user-ca -f ~/.ssh/ca
sed 's/^/cert-authority /' ~/.ssh/ca.pub > ~/.ssh/authorized_keys

# Generate or import a key in PIV slot 9c that requires touch.
# Import key and certificate:
yubico-piv-tool -s 9c -a import-key -i key.pem --pin-policy=never --touch-policy=always
yubico-piv-tool -s 9c -a import-certificate -i cert.pem

# Generate a key and sign a certificate. (Note the second operation will require touch)
yubico-piv-tool -a generate -s 9c -A RSA2048 --pin-policy=never --touch-policy=always -o public.pem
yubico-piv-tool -a selfsign-certificate -s 9c -S "/CN=SSH key/" -i public.pem -o cert.pem
yubico-piv-tool -a import-certificate -s 9c -i cert.pem

# Double check that the key appears in slot 9c (optional)
yubico-piv-tool -a status

# Clear the SSH agent
ssh-add -D
ssh-add -e /PATH/TO/libykcs11.so

# Make sure to use the correct path (e.g. /usr/local/lib) and extension (.so for Linux, .dylib for Mac OS X) for libykcs11.
# Beware that ssh-add -D doesn�t seem to clear PKCS#11 libraries, only keys (link).
# Add PIV to the SSH agent (will prompt for PIV PIN)
ssh-add -s /PATH/TO/libykcs11.so

# Get the public key from PIV and sign it using the CA key. This produces ~/.ssh/id_rsa-cert.pub
ssh-add -L > ~/.ssh/id_rsa.pub
ssh-keygen -s ~/.ssh/ca -I identity -n "${LOGNAME}" ~/.ssh/id_rsa.pub

# Authenticate to the target system using the new key:
ssh user@remote.example.com