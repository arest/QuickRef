
# Test and solution for overruns on IF
#
# On Server side:
iperf -s -B [SERVER_ONAPPSTORESAN_IF_IP_ADDRESS]

# On Client side:
iperf -c [SERVER_ONAPPSTORESAN_IF_IP_ADDRESS] -l 64K -d -P 10


# add to custom config of each HV
(sleep 600;for i in `ls -1 /sys/class/net/onappstoresan/brif | grep -v onapp | grep -v eth`; do ethtool -K $i tso off; done)&

# run on each HV
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
