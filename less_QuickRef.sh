#!/bin/bash
echo === less ===
cat <<EOF
    / – search for a pattern which will take you to the next occurrence.
    n – for next match in forward
    N – for previous match in backward


    ? – search for a pattern which will take you to the previous occurrence.
    n – for next match in backward direction
    N – for previous match in forward direction


    CTRL+F – forward one window
    CTRL+B – backward one window
    CTRL+D – forward half window
    CTRL+U – backward half window


    j – navigate forward by one line
    k – navigate backward by one line


    G – go to the end of file
    g – go to the start of file
    q or ZZ – exit the less pager


    10j – 10 lines forward.
    10k – 10 lines backward.
    CTRL+G – show the current file name along with line, byte and percentage statistics.


    v – using the configured editor edit the current file.
    h – summary of less commands
    &pattern – display only the matching lines, not all.


    ma – mark the current position with the letter ‘a’,
    ‘a – go to the marked position ‘a’.
EOF
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
