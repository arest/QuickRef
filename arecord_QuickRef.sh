arecord -q -f cd -d 1 recvol.wav;sox recvol.wav -n stat 2>&1|grep RMS|grep amplitude|cut -d"." -f2|cut -c 1-2>recvol;echo $((`cat recvol`+1))>recvol;rec -t wav - silence 1 0.1 `cat recvol` -1 1.0 `cat recvol`%|lame -s 44.1 -a -v - >record.mp3

arecord -D plughw:0,0 -f S16_LE -c1 -r22050 -t raw | lame -r -s 22.05 -m m -b 64 - mic-input.mp3

ffmpeg -f alsa -ac 2 -i pulse -acodec pcm_s16le -vcodec libx264 -vpre lossless_ultrafast -threads 0 -y myVOICE.wav 
lame -r -s 22.05 -m m -b 64 myVOICE.wav  mic-input.mp3


# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
