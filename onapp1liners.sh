# Useful one-liners used in OnApp environment

# Removing backup snapshots
echo 'select ip_address from hypervisors;select ip_address from backup_servers;' | mysql -uroot -pcat /onapp/interface/config/database.yml | grep password | head -1 | awk '{print $2}' | sed 's/"//g' onapp | grep -v ip_address | while read hv;do echo $hv; echo -e "lvscan 2>/dev/null | grep backup | grep ACTIVE | awk '{match(\$0,\"/dev/onapp-[a-z0-9]/backup-[a-z0-9.]\"); print \$0; system(\"lvremove -f \"substr(\$0,RSTART,RLENGTH))}' 2>&1" | ssh root@$hv 2>/dev/null; done | grep -v duplicate |
grep -v "read failed"

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
